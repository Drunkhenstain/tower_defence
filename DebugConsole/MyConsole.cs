﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game1
{
    public static class MyConsole
    {
        private static Dictionary<string, string> _logHistory = new Dictionary<string, string>();

        private static int _maxCharsInLog;

        private static Stopwatch _loopTimer = new Stopwatch();

        public static void Main(string[] args)
        {
        }

        public static void Start()
        {
            _loopTimer.Start();
            _maxCharsInLog = 1;
            for (int i = 0; i < 10; i++)
            {
                AddLog(i.ToString(), "Haaaii" + i);
            }
        }


        private static void AddLog(string key, string text)
        {
            if (text.Length > _maxCharsInLog)
            {
                _maxCharsInLog = text.Length;
            }
            _logHistory.Add(key, text);
        }

        static void DrawLog()
        {
                        int logEntrys = _logHistory.Count;
                        if (logEntrys > Console.LargestWindowHeight)
                        {
                            logEntrys = Console.LargestWindowHeight;
                        }


                        int countLogs = 0;
                        foreach (string log in _logHistory.Values)
                        {
                            for (int x = 0; x < _maxCharsInLog; x++)
                            {
                                string cuChar;
                                if (x < log.Length)
                                {
                                    cuChar = log[x].ToString();
                                }
                                else
                                {
                                    cuChar = " ";
                                }
                                DrawCharAt(x, countLogs, cuChar);
                            }
                            countLogs++;
                        }
                        _loopTimer.Restart();
                    
        
        }
        private static void DrawCharAt(int x, int y, string letter)
        {
            try
            {
                x += 1;
                y += 1;
                Console.SetCursorPosition(x, y);
                Console.Write(letter);

            }
            catch (Exception)
            {
                Console.WriteLine("what");
            }
        }
    }
}
