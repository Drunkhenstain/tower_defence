﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Game1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private KeyboardState keyboard_previousState;
        private FrameCounter _frameCounter = new FrameCounter();

        private Stopwatch _timer = new Stopwatch();

        GameTime _gameTime;

        int minFps = 61;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.IsFullScreen = true;
            //this.IsMouseVisible = true;
            TargetElapsedTime = TimeSpan.FromTicks(166666); //60fps to get updates at 60fps and PresentationInterval = PresentInterval.Two
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            keyboard_previousState = Keyboard.GetState();
            graphics.PreferredBackBufferWidth = 1800;
            graphics.PreferredBackBufferHeight = 1000;



            graphics.ApplyChanges();
            
            _timer.Start();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // Load Images/Sprites
            FontSpriteManager.testBackground = Content.Load<Texture2D>("images/background");
            FontSpriteManager.testBackground2 = Content.Load<Texture2D>("images/background2");
            FontSpriteManager.largeBackground = Content.Load<Texture2D>("images/backgroundLarge");
            FontSpriteManager.largeBackground2 = Content.Load<Texture2D>("images/background2Large");
            FontSpriteManager.largeBackground3 = Content.Load<Texture2D>("images/background3Large");
            FontSpriteManager.largeBackground4 = Content.Load<Texture2D>("images/background4Large");
            FontSpriteManager.largeBackground5   = Content.Load<Texture2D>("images/background5Large");
            FontSpriteManager.largeBackground6 = Content.Load<Texture2D>("images/background6Large");
            FontSpriteManager.largeBackground7 = Content.Load<Texture2D>("images/background7Large");


            FontSpriteManager.uglybaxa = Content.Load<Texture2D>("images/uglybaxa");

            FontSpriteManager.creep1 = Content.Load<Texture2D>("images/eyebot");
            FontSpriteManager.ship1 = Content.Load<Texture2D>("images/ship1");
            FontSpriteManager.ship2 = Content.Load<Texture2D>("images/ship2");
            FontSpriteManager.ship3 = Content.Load<Texture2D>("images/ship3");
            FontSpriteManager.ship4 = Content.Load<Texture2D>("images/shipbeta");
            FontSpriteManager.ship5 = Content.Load<Texture2D>("images/shipalpha");

            FontSpriteManager.userShip = Content.Load<Texture2D>("images/spaceship");

            FontSpriteManager.LightningSegment = Content.Load<Texture2D>("images/Lightning Segment");
            FontSpriteManager.HalfCircle = Content.Load<Texture2D>("images/Half Circle");


            FontSpriteManager.explosion5x5 = Content.Load<Texture2D>("images/slime");
            FontSpriteManager.circle = Content.Load<Texture2D>("images/circle");
            FontSpriteManager.triangle = Content.Load<Texture2D>("images/dreieck");

            FontSpriteManager.asteroid = Content.Load<Texture2D>("images/asteroid");
            FontSpriteManager.earth = Content.Load<Texture2D>("images/earth");
            FontSpriteManager.bullet = Content.Load<Texture2D>("images/projectile");


            FontSpriteManager.rawWhite = Content.Load<Texture2D>("images/white");
            FontSpriteManager.blueCircle = Content.Load<Texture2D>("images/blue");
            FontSpriteManager.redCircle = Content.Load<Texture2D>("images/red");
            FontSpriteManager.greenCircle = Content.Load<Texture2D>("images/green");

            // Load Fonts
            FontSpriteManager.fontMid = Content.Load<SpriteFont>("fonts/scoreMid");
            FontSpriteManager.fontSmall = Content.Load<SpriteFont>("fonts/scoreSmall");
            FontSpriteManager.fontExtraSmall = Content.Load<SpriteFont>("fonts/scoreExtraSmall");

            // Load GameManager
            GameManager.InitializeGame();
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            // If they hit esc, exit
            _gameTime = gameTime;
            if (GameManager.ExitGame == true ||GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            if (InputManager.GetSingleKeypress(Keys.F12))
            {
                graphics.IsFullScreen = !graphics.IsFullScreen;
                graphics.ApplyChanges();
            }

            if (_timer.ElapsedMilliseconds > 1000)
            {
                GameManager.Update(gameTime);
            }
            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            _frameCounter.Update(deltaTime);

            spriteBatch.GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            if (_timer.ElapsedMilliseconds > 1000)
            {
                if (_frameCounter.AverageFramesPerSecond < minFps)
                {
                    minFps = (int)_frameCounter.AverageFramesPerSecond;
                }
                GameManager.Draw(spriteBatch);
                spriteBatch.DrawString(FontSpriteManager.fontSmall, "FPS:     " + _frameCounter.AverageFramesPerSecond, new Vector2(1610, 1), Color.WhiteSmoke);
                spriteBatch.DrawString(FontSpriteManager.fontSmall, "MIN FPS: " + minFps, new Vector2(1610, 25), Color.WhiteSmoke);
            }
            else
            {
                spriteBatch.DrawString(FontSpriteManager.fontSmall, "FPS:     " + _frameCounter.AverageFramesPerSecond, new Vector2(1, 1), Color.WhiteSmoke);
                spriteBatch.DrawString(FontSpriteManager.fontSmall, "MIN FPS: " + minFps, new Vector2(1, 25), Color.WhiteSmoke);
                spriteBatch.DrawString(FontSpriteManager.fontSmall, "Wait", new Vector2(500, 500), Color.WhiteSmoke);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
