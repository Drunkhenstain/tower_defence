﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    public class Button
    {
        private GameActor _owner;

        private Vector2 _position;
        private Vector2 _size;

        private Color _color;

        private Texture2D _sprite;

        private bool _mouseOver=false;
        private bool _wasPressed;
        private bool _isPressed;
        private bool _isHold;


        private string _text = "";
        private string _buttonType;


        private int _timesClicked = 0;

        private int _clickProgress = 0;
        private float _clicksNeeded = 100;

        public Button(Vector2 position, Vector2 size, string buttonType, string text, Color color)
        {
            _text = text;
            _position = position;
            _size = size;
            _color = color;
            _isPressed = false;
            _buttonType = buttonType;
            _owner = null;
            Initialize();
        }
        public Button(Vector2 position, Vector2 size, string buttonType, string text, int clicksNeeded, Color color)
        {
            _text = text;
            _position = position;
            _size = size;
            _color = color;
            _isPressed = false;
            _buttonType = buttonType;
            _clicksNeeded = clicksNeeded;
            Initialize();
        }

        public Button(GameActor owner, Vector2 size, string buttonType, string text, Color color)
        {
            _text = text;
            _owner = owner;
            _size = size;
            _color = color;
            _isPressed = false;
            _buttonType = buttonType;
            Initialize();

        }
        public Button(GameActor owner, Vector2 size, string buttonType, string text, int clicksNeeded, Color color)
        {
            _text = text;
            _owner = owner;
            _size = size;
            _color = color;
            _isPressed = false;
            _buttonType = buttonType;
            _clicksNeeded = clicksNeeded;
            Initialize();
        }
        
        public Button(Vector2 position, Vector2 size, string buttonType, string text, Texture2D sprite, Color textColor)
        {
            _text = text;
            _position = position;
            _size = size;
            _sprite = sprite;
            _isPressed = false;
            _buttonType = buttonType;
            _color = textColor;
            _owner = null;
            Initialize();
        }
        public Button(Vector2 position, Vector2 size, string buttonType, string text, Texture2D sprite, int clicksNeeded, Color textColor)
        {
            _text = text;
            _position = position;
            _size = size;
            _sprite = sprite;
            _isPressed = false;
            _buttonType = buttonType;
            _color = textColor;
            _owner = null;
            _clicksNeeded = clicksNeeded;
            Initialize();
        }

        private void Initialize()
        {
            if (_buttonType.Contains("hold"))
            {
                _isHold = true;
            }
        }

        public void Update()
        {
            if (_owner != null)
            {
                _position = _owner.DrawingCenter() - (_size / 2);
            }

            if (_isHold == true)
            {
                if (_wasPressed == false)
                {
                    if (_clickProgress > 0)
                    {
                        _clickProgress=0;
                    }
                }
            }

            MouseState mouse = InputManager.GetMouse();
            int mX = mouse.X;
            int mY = mouse.Y;
            _isPressed = false;
            if (CollisionHelper.BoundingRectangle(mX, mY, 1, 1, (int)_position.X, (int)_position.Y, (int)_size.X, (int)_size.Y))
            {
                _mouseOver = true;
            }
            else
            {
                _mouseOver = false;
            }
                if (InputManager.IsLeftMousePressed() && (_wasPressed == false || _isHold == true))
            {
                if (CollisionHelper.BoundingRectangle(mX, mY, 1, 1, (int)_position.X, (int)_position.Y, (int)_size.X, (int)_size.Y))
                {
                    if (_clickProgress < _clicksNeeded)
                    {
                        _clickProgress++;
                    }
                    _timesClicked++;
                    _isPressed = true;
                }
                else
                {
                    _isPressed = false;
                    _clickProgress = 0;
                }
            }
            _wasPressed = InputManager.IsLeftMousePressed();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Vector2 extraSmallHoldTextOffset = new Vector2(-1, -4);
            switch (_buttonType)
            {
                case "extrasmall":
                    DrawHelper.DrawExtraSmallTextWithLabel(spriteBatch, _position, _size, _text, 1, _color);
                    break;

                case "small":
                    DrawHelper.DrawSmallTextWithLabel(spriteBatch, _position, _size, _text, 1, _color);
                    break;

                case "normal":
                    DrawHelper.DrawTextWithLabel(spriteBatch, _position, _size, _text, 1, _color);
                    break;

                case "extrasmall hold":
                    DrawHelper.DrawProgressBar(spriteBatch, _position, _size, _clickProgress / _clicksNeeded, 1, Color.Silver, _color);
                    DrawHelper.DrawExtraSmallText(spriteBatch, _text, _position+ extraSmallHoldTextOffset, 1, 0, Color.Black);
                    break;

                case "small hold":
                    DrawHelper.DrawProgressBar(spriteBatch, _position, _size, _clickProgress / _clicksNeeded, 1, Color.Silver, _color);
                    DrawHelper.DrawExtraSmallText(spriteBatch, _text, _position, 1, 0, Color.Black);
                    break;


                case "hold":
                    DrawHelper.DrawProgressBar(spriteBatch, _position, _size, _clickProgress / _clicksNeeded, 1, Color.Silver, _color);
                    DrawHelper.DrawText(spriteBatch, _text, _position, 1, 0, Color.Black);
                    break;

                case "sprite":
                    Rectangle dest = new Rectangle(_position.ToPoint(), _size.ToPoint());
                    spriteBatch.Draw(_sprite, dest, Color.White);
                    DrawHelper.DrawText(spriteBatch, _text, _position, 1, 0, _color);
                    break;

                default:
                    DrawHelper.DrawTextWithLabel(spriteBatch, _position, _size, _text, 1, _color);
                    break;
            }
        }

        public bool GetsInput()
        {
            if (_isHold == true)
            {
                if(_clickProgress>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return _isPressed;
            }
        }

        public bool IsClicked()
        {
            if (_isHold == true)
            {
                if (_clickProgress >= _clicksNeeded)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return _isPressed;
            }
        }

        public bool MouseOver()
        {
            return _mouseOver;
        }
    }
}
