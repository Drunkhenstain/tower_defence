﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    public class LightningEmitter
    {
        List<LightningBolt> _activeBolts = new List<LightningBolt>();
        Vector2 _position;
        Color _color;
        float _fadeOutRate;

        private bool IsActive()
        {
            if (_activeBolts.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public LightningEmitter(Vector2 position, float fadeOutRate)
        {
            _position = position;
            _fadeOutRate = fadeOutRate;
        }

        public void AddTarget(Vector2 target)
        {
            if (_activeBolts.Count < 1000)
            {
                _activeBolts.Add(new LightningBolt(_position, target, _fadeOutRate, _color));
            }
        }
        public void AddTarget(Vector2 target, Color color)
        {
            if (_activeBolts.Count < 1000)
            {
                _activeBolts.Add(new LightningBolt(_position, target, _fadeOutRate, color));
            }
        }
        public void AddTarget(Vector2 target, Color color, float fadeOutRate)
        {
            if (_activeBolts.Count < 1000)
            {
                _activeBolts.Add(new LightningBolt(_position, target, fadeOutRate, color));
            }
        }
        public void ClearTargets()
        {
            if (_activeBolts.Count > 0)
            {
                _activeBolts.Clear();
            }
        }

        public void Update(Vector2 position, Color color)
        {
            if (IsActive() == true)
            {
                if (color != Color.Transparent)
                {
                    _color = color;
                }
                else
                {
                    _color = DrawHelper.GetRandomColor();
                }
                _position = position;
                List<LightningBolt> tempRemove = new List<LightningBolt>();

                foreach (LightningBolt bolt in _activeBolts)
                {
                    bolt.Update();
                    if (bolt.IsComplete == true)
                    {
                        tempRemove.Add(bolt);
                    }
                }

                foreach (LightningBolt bolt in tempRemove)
                {
                    _activeBolts.Remove(bolt);
                }
            }
        }


        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.End();
            spriteBatch.Begin(SpriteSortMode.Texture, BlendState.Additive);
            for (int index = 0; index < _activeBolts.Count; index++)
            {
                _activeBolts[index].Draw(spriteBatch);
            }
            spriteBatch.End();
            spriteBatch.Begin();
        }
    }
}
