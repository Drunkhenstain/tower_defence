﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public class ReachGoalEmitter
    {
        private static Random random;
        public Vector2 EmitterLocation { get; set; }
        private List<Particle> _particles;
        private List<Texture2D> _textures;

        public ReachGoalEmitter(List<Texture2D> textures,float size,int ttl, Vector2 location)
        {
            random = new Random();
            EmitterLocation = location;
            this._textures = textures;
            _particles = new List<Particle>();
        }
        public ReachGoalEmitter(List<Texture2D> textures, Vector2 location)
        {
            random = new Random();
            EmitterLocation = location;
            this._textures = textures;
            _particles = new List<Particle>();
        }

        public void Update(Vector2 location, Vector2 towerSpeed, Color color)
        {
            int total = 7;
            EmitterLocation = location;
            for (int i = 0; i < total; i++)
            {
                _particles.Add(GenerateNewParticle(towerSpeed, color));
            }

            for (int particle = 0; particle < _particles.Count; particle++)
            {
                _particles[particle].Update();
                if (_particles[particle].MaxLifespan <= 0)
                {
                    _particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        private Particle GenerateNewParticle(Vector2 projectileSpeed, Color color)
        {
            Texture2D texture = _textures[random.Next(_textures.Count)];
            Vector2 position = EmitterLocation;
            Vector2 velocity = new Vector2(1f * (float)(random.NextDouble() * 2 - 1), 1f * (float)(random.NextDouble() * 2 - 1));
            Vector2 particleSpeed = new Vector2(random.Next(3,6));

            velocity *= particleSpeed;

            float angle = MyMathHelper.GetRandom(0, 360);
            float angularVelocity = 0;
            float size = 1.7f;
            int ttl = 10;

            return new Particle(texture, position, velocity, angle, angularVelocity, color, size, ttl);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int index = 0; index < _particles.Count; index++)
            {
                _particles[index].Draw(spriteBatch);
            }
        }
    }
}