﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public class ThrusterEmitter
    {
        private static Random random;
        public Vector2 EmitterLocation { get; set; }
        private List<Particle> _particles;
        private List<Texture2D> _textures;

        public ThrusterEmitter(List<Texture2D> textures, Vector2 location)
        {
            random = new Random();
            EmitterLocation = location;
            _textures = textures;
            _particles = new List<Particle>();
        }

        public void Update(Vector2 location, Color color)
        {
            int total = 2;
            EmitterLocation = location;
            for (int i = 0; i < total; i++)
            {
                _particles.Add(GenerateNewParticle(color));
            }

            for (int particle = 0; particle < _particles.Count; particle++)
            {
                _particles[particle].Update();
                if (_particles[particle].MaxLifespan <= 0)
                {
                    _particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        private Particle GenerateNewParticle(Color color)
        {
            Texture2D texture = _textures[random.Next(_textures.Count)];
            Vector2 position = EmitterLocation;
            Vector2 velocity = new Vector2(1f * (float)(random.NextDouble() * 2 - 1), 1f * (float)(random.NextDouble() * 2 - 1));

            velocity *= 0.7f;
            float angle = 0;
            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);
            float size = 0.17f;
            int ttl = 20;
            position += new Vector2(random.Next(4));

            return new Particle(texture, position, velocity, angle, angularVelocity, color, size, ttl);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int index = 0; index < _particles.Count; index++)
            {
                _particles[index].Draw(spriteBatch);
            }
        }
    }
}