﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public class BasicCreep : Creep
    {

        private bool _switchMotion = true;
        private bool _accelerate = false;
        private float _motionModifier = 0;
        private int _timespanMotion = 1;
        private int _timespanFrameCount = 0;



        public BasicCreep(Vector2 position, Texture2D sprite, string id, int actorType, int lane) : base(position, sprite, id, actorType, lane)
        {
            _coinsDropped = 0;

            _minSpeed = 24f;
            _maxSpeed = 27f;
            //_maxDeadTime = 30;

            _armor = 5;
            _minHealth = 1500;
            _maxHealth = 1900;

            _maxTimespanGetHit = 18;
            _timeSinceHit = 0;

            _defaultHealth = MyMathHelper.GetRandom((int)_minHealth, (int)_maxHealth);
            _healthPoints = _defaultHealth;

            int rndSize = MyMathHelper.GetRandom(27, 40) + (int)_healthPoints / 100;
            _drawingSquare.Size = new Point(rndSize, rndSize - 10);

            _defaultSpeed = MyMathHelper.GetRandom((int)_minSpeed, (int)_maxSpeed);
            _currentSpeed = _defaultSpeed;
            _speed = _currentSpeed;

            _defaultColor = DrawHelper.GetRandomColor();
            _maskColor = _defaultColor;

            _hurtColor = Color.Red;
            _switchMotion = true;
        }

        public override void Initialize(SvgDataContainer logic, bool isEven, int levelHardness, int startDelay)
        {
            _startDelay = startDelay;
            _coinsDropped = MyMathHelper.GetRandom(3, 6);
            _defaultHealth += (int)((_defaultHealth / 14) * 4);/// testieee
            _healthPoints = _defaultHealth;
            base.Initialize(logic, isEven, levelHardness, startDelay);
        }

        public override void UpgradeCreep(float extraSpeed, float extraLive, float extraArmor, int currentWave)
        {
            base.UpgradeCreep(extraSpeed, extraLive, extraArmor, currentWave);
            if (currentWave > 4)
            {
                _shield = 3;               
                _shieldChargeRate = 800;
                _maxShield = _shield;
            }
        }

        public override void Update(GameTime gameTime)
        {
            _switchMotion = false;              /// for constant speed

            _timeSinceSpawn++;
            if (_timeSinceSpawn >= _startDelay)
            {

                base.Update(gameTime);
                if (_isFreezed == false)
                {
                    SwitchMotion();
                }
            }
        }

        private void SwitchMotion()
        {
            if (_switchMotion == true)
            {
                if (_accelerate == true)
                {
                    _accelerate = false;
                }
                else
                {
                    _accelerate = true;
                }
                _timespanMotion = MyMathHelper.GetRandom(300, 900);
                _motionModifier = (float)MyMathHelper.GetRandom(33, 333) / 3000;
                _switchMotion = false;
            }

            if (_accelerate == true)
            {
                if (_speed < _maxSpeed)
                    _speed += _motionModifier;
            }
            else
            {
                if (_speed > _minSpeed)
                    _speed -= _motionModifier;
            }

            if (_timespanFrameCount < _timespanMotion)
            {
                _timespanFrameCount++;
            }
            else
            {
                _switchMotion = true;
                _timespanFrameCount = 0;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
