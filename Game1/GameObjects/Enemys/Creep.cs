﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public abstract class Creep : GameActor
    {
        protected int _minFreezedSpeed;
        protected int _lane;
        protected int _nextLanePoint = 1;
        protected int _defaultHealth;
        protected int _pointCount;
        protected int _coinsDropped;

        protected int _timeSinceSpawn;
        protected int _startDelay;

        protected int _timeSinceFreeze;
        protected int _maxFreezeTime;

        protected int _timeSinceHit;
        protected int _maxTimespanGetHit;

        protected int _timeSinceDead;
        protected int _maxDeadTime;

        protected float _healthPoints;
        protected float _minHealth;
        protected float _maxHealth;

        protected float _maxSpeed;
        protected float _minSpeed;

        protected float _armor;

        protected float _shieldChargeRate;
        protected float _curShieldCharge;
        protected int _maxShield;
        protected int _shield;

        protected float _defaultSpeed;
        protected float _currentSpeed;
        protected float _distance;

        protected bool _gotHit;
        protected bool _isDead;
        protected bool _isFreezed;
        public bool _isStunned;
        public bool ReachedGoal = false;

        protected Vector2 _pointA;
        protected Vector2 _pointB;

        protected Color _hurtColor;

        SvgDataContainer _logicInfo;

        protected DeadEmitter _Emitter;
        protected ReachGoalEmitter _goalEmitter;
        protected ThrusterEmitter _thrustEmitter;

        public Creep(Vector2 position, Texture2D sprite, string id, int actorType, int lane) : base(position, sprite, id, actorType)
        {
            _pointA = position;
            _lane = lane;
            _isStunned = false;
            _isDead = false;
            _isActive = true;
            _maxDeadTime = 60;
            _minFreezedSpeed = 3;

            _timeSinceHit = 0;
            List<Texture2D> tempSprites = new List<Texture2D>();
            tempSprites.Add(FontSpriteManager.triangle);
            tempSprites.Add(FontSpriteManager.circle);
            _Emitter = new DeadEmitter(tempSprites, DrawingCenter());
            _goalEmitter = new ReachGoalEmitter(tempSprites, DrawingCenter());
            //_thrustEmitter = new ThrusterEmitter(tempSprites, DrawingCenter());
        }

        public virtual void Initialize(SvgDataContainer logic, bool updateEvenFrame, int levelHardness, int startDelay)
        {
            _shield = _maxShield;
            _defaultColor = _maskColor;
            _updateEvenFrame = updateEvenFrame;
            _logicInfo = logic;
            _pointB = _logicInfo.Paths[_lane].polylines[_nextLanePoint];
            _pointCount = _logicInfo.Paths[_lane].polylines.Count();
            _distance = MyMathHelper.CalcDistance(_pointA, _pointB);
            _direction = MyMathHelper.CalcDirection(_pointA, _pointB, _distance);
            _angle = MyMathHelper.GetAngle(_position, _pointB);
        }

        public virtual void UpgradeCreep(float extraSpeed, float extraLive, float extraArmor, int currentWave)
        {
            _defaultSpeed += extraSpeed;
            _currentSpeed = _defaultSpeed;
            _speed = _currentSpeed;

            _defaultHealth += (int)extraLive;
            _healthPoints = _defaultHealth;

            _armor += extraArmor;
            if (currentWave < 6)
            {
                _coinsDropped += currentWave - 2;
            }
            else 
            {
                _coinsDropped += 4;
            }
        }

        public override void Update(GameTime gameTime)
        {
            UpdateEffects();
            /// update shield recharge if neccessary and only when NOT freezed
            ///
            if (_isFreezed == false)
            {
                if (_shield == 0 && _maxShield > 0)
                {
                    _curShieldCharge++;
                }
            }
            if (_maxShield > 0)
            {
                if (_curShieldCharge >= _shieldChargeRate)
                {
                    _shield = _maxShield;
                    _curShieldCharge = 0;
                }
            }
            /// initiate death animation when health <=0
            /// 
            if (_healthPoints <= 0)
            {
                _isActive = false;
                _timeSinceDead++;
                if (ReachedGoal == false)
                {
                    _Emitter.Update(DrawingCenter(), new Vector2(0, 0), _maskColor);
                }
                else
                {
                    _goalEmitter.Update(DrawingCenter(), new Vector2(0, 0), Color.Gold);
                }
            }
            /// give the creep true death after death animation
            /// 
            if (_timeSinceDead >= _maxDeadTime)
            {
                _isDead = true;
            }
            /// start invasion when set to active
            /// 
            if (_isActive == true)
            {
                UpdatePosition(gameTime);
            }
            /// when it reaches destination
            /// ...
            if (MyMathHelper.CalcDistance(_position, _pointA) >= _distance)
            {
                GetNextWaypoint();
            }
            /// update base stuff
            /// 
            if (_thrustEmitter != null)
            {
                _thrustEmitter.Update(_position, _maskColor);

            }
            base.Update(gameTime);
        }

        private void UpdateEffects()
        {
            /// check for freeze effect
            /// 
            if (_isFreezed == true)
            {
                _timeSinceFreeze++;
                _maskColor = Color.DodgerBlue;
                if (_timeSinceFreeze >= _maxFreezeTime)
                {
                    _speed = _defaultSpeed;
                    _timeSinceFreeze = 0;
                    _maxFreezeTime = 0;
                    _isFreezed = false;
                    _isStunned = false;
                }
            }

            if (_gotHit == true)
            {
                _timeSinceHit++;
                _maskColor = Color.Red;
                if (_timeSinceHit >= _maxTimespanGetHit)
                {
                    _gotHit = false;
                    _timeSinceHit = 0;
                }
            }

            if (_isStunned == true)
            {
                _maskColor = Color.MonoGameOrange;
            }

            if (_isFreezed == false && _gotHit == false && _isStunned == false)
            {
                _maskColor = _defaultColor;
            }
        }

        private void UpdatePosition(GameTime gameTime)
        {
            _position = MyMathHelper.CalcSmoothMovement(_position, _direction, _speed, (float)gameTime.ElapsedGameTime.TotalSeconds);
        }

        private void GetNextWaypoint()
        {
            _nextLanePoint++;
            _position = _pointB;
            _pointA = new Vector2(_logicInfo.Paths[_lane].polylines[_nextLanePoint - 1].X, _logicInfo.Paths[_lane].polylines[_nextLanePoint - 1].Y);
            _pointB = new Vector2(_logicInfo.Paths[_lane].polylines[_nextLanePoint].X, _logicInfo.Paths[_lane].polylines[_nextLanePoint].Y);

            _distance = MyMathHelper.CalcDistance(_pointA, _pointB);
            _direction = MyMathHelper.CalcDirection(_pointA, _pointB, _distance);
            _position = _pointA;
            _angle = MyMathHelper.GetAngle(_position, _pointB);

            _angleOrigin = new Vector2(_drawingSquare.Width / 2, _drawingSquare.Width / 2);

            if (_nextLanePoint == _pointCount - 1)
            {
                _nextLanePoint = 0;
                _healthPoints = 0;
                ReachedGoal = true;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_timeSinceDead > 0)
            {
                if (ReachedGoal == false)
                {
                    _Emitter.Draw(spriteBatch);
                    DrawHelper.DrawText(spriteBatch, "$" + _coinsDropped + "$", _position, 2, 0, Color.White);
                }
                else
                {
                    _goalEmitter.Draw(spriteBatch);
                }
            }
            else
            {
                if (_thrustEmitter != null)
                {
                    _thrustEmitter.Draw(spriteBatch);
                }
                base.Draw(spriteBatch);
                DrawHealthbar(spriteBatch);

                if (_shield > 0)
                {
                    DrawShieldbar(spriteBatch);
                }
            }
        }

        protected virtual void DrawHealthbar(SpriteBatch spriteBatch)
        {
            float percentProg;
            percentProg = (_healthPoints / _defaultHealth);
            DrawHelper.DrawProgressBar(spriteBatch, _position, new Vector2(_drawingSquare.Width, 7), percentProg, 1f);
        }

        protected virtual void DrawShieldbar(SpriteBatch spriteBatch)
        {
            float percentProgShield;
            percentProgShield = (float)_shield / _maxShield;
            DrawHelper.DrawProgressBar(spriteBatch, _position, new Vector2(_drawingSquare.Width, 7), percentProgShield, 1f, Color.Azure, Color.Transparent);

            for (int i = 0; i < _maxShield; i++)
            {
                DrawHelper.DrawEmptyRectangle(spriteBatch, _position, new Vector2((_drawingSquare.Width / _maxShield) * (i + 1), 7), 1, Color.Black);
            }
        }

        public void GetStunned(int stunTime)
        {
            _maxFreezeTime = stunTime;
            _speed = _minFreezedSpeed;
            _isStunned = true;
            _isFreezed = true;
        }

        public virtual void GetDamage(Projectile projectile)
        {
            if (_shield > 0)
            {
                _shield--;
            }
            else
            {
                switch (projectile.Id())
                {
                    case "singletarget":
                        ProcessSingleTarget(projectile);
                        break;

                    case "singlefreeze":
                        if (_isStunned == false)
                        {
                            ProcessSingleFreeze(projectile);
                        }
                        break;

                    case "splash":
                        ProcessSplash(projectile);
                        break;
                }

            }
        }

        protected virtual void ProcessSingleTarget(Projectile projectile)
        {
            float dmg = projectile.Damage();
            dmg -= _armor;

            if (dmg <= 0)
            {
                dmg = 1;
            }
            _healthPoints -= dmg;
            _maskColor = _hurtColor;
            _timeSinceHit = 0;
            _gotHit = true;
        }

        protected virtual void ProcessSplash(Projectile projectile)
        {
            float dmg = projectile.Damage();
            dmg -= _armor;

            if (dmg <= 0)
            {
                dmg = 1;
            }
            _healthPoints -= dmg;
            _maskColor = _hurtColor;
            _timeSinceHit = 0;
            _gotHit = true;
        }

        protected virtual void ProcessSingleFreeze(Projectile projectile)
        {
            _isFreezed = true;
            if (_maxFreezeTime < projectile.EffectDuration)
            {
                _maxFreezeTime = projectile.EffectDuration;
            }

            if (_speed - projectile.Damage() <= _minFreezedSpeed)
            {
                _speed = _minFreezedSpeed;
            }
            else
            {
                _speed -= projectile.Damage();
            }
        }


        public float Health()
        {
            return _healthPoints;
        }
        public bool IsDead()
        {
            return _isDead;
        }
        public int Loot()
        {
            if (_coinsDropped != 0)
            {
                int temp = _coinsDropped;
                _coinsDropped = 0;
                return temp;
            }
            else
            {
                return 0;
            }
        }
    }
}
