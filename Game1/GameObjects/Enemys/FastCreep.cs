﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Game1
{
    public class FastCreep : Creep
    {
        private float _frostWeakness = 2;
        public FastCreep(Vector2 position, Texture2D sprite, string id, int actorType, int lane) : base(position, sprite, id, actorType, lane)
        {
            _coinsDropped = 0;

            _minSpeed = 38f;
            _maxSpeed = 42f;
            //_maxDeadTime = 30;

            _maxShield = 0;
            _shieldChargeRate = 800;
            _armor = 2;
            _minHealth = 1000;
            _maxHealth = 1300;

            _maxTimespanGetHit = 18;
            _timeSinceHit = 0;

            _defaultHealth = MyMathHelper.GetRandom((int)_minHealth, (int)_maxHealth);
            _healthPoints = _defaultHealth;

            int rndSize = MyMathHelper.GetRandom(15, 25) + (int)_healthPoints / 200;
            _drawingSquare.Size = new Point(rndSize, rndSize - 10);

            _defaultSpeed = MyMathHelper.GetRandom((int)_minSpeed, (int)_maxSpeed);
            _currentSpeed = _defaultSpeed;
            _speed = _currentSpeed;

            //_defaultColor = new Color(255, 229, 0);
            _defaultColor = DrawHelper.GetRandomColor();
            _maskColor = _defaultColor;

            _hurtColor = Color.Red;

            List<Texture2D> tempSprites = new List<Texture2D>();
            tempSprites.Add(FontSpriteManager.triangle);
            tempSprites.Add(FontSpriteManager.circle);
            _thrustEmitter = new ThrusterEmitter(tempSprites, DrawingCenter());

        }

        public override void Initialize(SvgDataContainer logic, bool isEven, int levelHardness, int startDelay)
        {
            _startDelay = startDelay;
            _coinsDropped = MyMathHelper.GetRandom(2, 5);
            _defaultHealth += (int)((_defaultHealth / 14) * 4);/// testieee
            _healthPoints = _defaultHealth;
            _shield += levelHardness;
            base.Initialize(logic, isEven, levelHardness, startDelay);
        }

        public override void UpgradeCreep(float extraSpeed, float extraLive, float extraArmor, int currentWave)
        {
            base.UpgradeCreep(extraSpeed,extraLive,extraArmor,currentWave);
            _shield += currentWave;
            _maxShield = _shield;
        }

        public override void Update(GameTime gameTime)
        {
            _timeSinceSpawn++;
            if (_timeSinceSpawn >= _startDelay)
            {
                base.Update(gameTime);

                if (_isFreezed == false)
                {

                }
            }
        }

        protected override void ProcessSingleFreeze(Projectile projectile)
        {
            _isFreezed = true;
            _maxFreezeTime = projectile.EffectDuration;

            if (_speed - projectile.Damage() * _frostWeakness <= _minFreezedSpeed)
            {
                _speed = _minFreezedSpeed* _frostWeakness;
            }
            else
            {
                _speed -= projectile.Damage()* _frostWeakness;
            }
        }
    }
}
