﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public class FatCreep : Creep
    {
        public FatCreep(Vector2 position, Texture2D sprite, string id, int actorType, int lane) : base(position, sprite, id, actorType, lane)
        {
            _coinsDropped = 0;

            _minSpeed = 20f;
            _maxSpeed = 23f;
            //_maxDeadTime = 30;

            _minHealth = 3600;
            _maxHealth = 4200;

            _armor = 35;

            _maxTimespanGetHit = 18;
            _timeSinceHit = 0;

            _defaultHealth = MyMathHelper.GetRandom((int)_minHealth, (int)_maxHealth);
            _healthPoints = _defaultHealth;

            int rndSize = MyMathHelper.GetRandom(50, 60) ;
            _drawingSquare.Size = new Point(rndSize , rndSize );

            _defaultSpeed = MyMathHelper.GetRandom((int)_minSpeed, (int)_maxSpeed);
            _currentSpeed = _defaultSpeed;
            _speed = _currentSpeed;

            _defaultColor = Color.DarkRed;
            _maskColor = _defaultColor;

            _hurtColor = Color.Red;
        }

        public override void Initialize(SvgDataContainer logic, bool isEven, int levelHardness, int startDelay)
        {
            _startDelay = startDelay;
            _coinsDropped = MyMathHelper.GetRandom(20, 30);
            //_defaultHealth += (int)((_defaultHealth / 14) * 4);/// testieee
            _healthPoints = _defaultHealth;
            base.Initialize(logic, isEven, levelHardness, startDelay);
        }

        public override void Update(GameTime gameTime)
        {
            _timeSinceSpawn++;
            if (_timeSinceSpawn >= _startDelay)
            {
                base.Update(gameTime);

                if (_isFreezed == false)
                {

                }
            }
        }

        protected override void ProcessSingleFreeze(Projectile projectile)
        {
            _isFreezed = true;
            _maxFreezeTime = 50;

            _speed = _minSpeed - 5;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
