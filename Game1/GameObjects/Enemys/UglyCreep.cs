﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public class UglyCreep : Creep
    {
        public UglyCreep(Vector2 position, Texture2D sprite, string id, int actorType, int lane) : base(position, sprite, id, actorType, lane)
        {
            _coinsDropped = 90;

            _minSpeed = 19f;
            _maxSpeed = 21f;

            _minHealth = 5000;
            _maxHealth = 6000;

            _armor = 50;
            _maxShield = 20;
            _shield = _maxShield;
            _shieldChargeRate = 350;

            _maxTimespanGetHit = 18;
            _timeSinceHit = 0;

            _defaultHealth = MyMathHelper.GetRandom((int)_minHealth, (int)_maxHealth);
            _healthPoints = _defaultHealth;

            _drawingSquare.Size = new Point(90, 60);

            _defaultSpeed = MyMathHelper.GetRandom((int)_minSpeed, (int)_maxSpeed);
            _currentSpeed = _defaultSpeed;
            _speed = _currentSpeed;

            _defaultColor = Color.White;
            _maskColor = _defaultColor;

            _hurtColor = Color.Red;
        }

        public override void Initialize(SvgDataContainer logic, bool isEven, int levelHardness, int startDelay)
        {
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Fat'n Ugly Baxa Incoming", Color.Pink);
            _startDelay = startDelay;
            _defaultHealth += (int)((_defaultHealth / 14) * 4);/// testieee
            _healthPoints = _defaultHealth;
            base.Initialize(logic, isEven, levelHardness, startDelay);
        }

        public override void Update(GameTime gameTime)
        {
            _timeSinceSpawn++;
            if (_timeSinceSpawn >= _startDelay)
            {
                base.Update(gameTime);

                if (_isFreezed == false)
                {
                    if (_speed < _maxSpeed + 2)
                    {
                        _speed += 0.1f;
                    }
                }
            }
        }

        protected override void ProcessSingleFreeze(Projectile projectile)
        {
            _isFreezed = true;
            if (projectile.Damage() > 50)
            {
                _maxFreezeTime = projectile.EffectDuration / 2;
                _speed = _minSpeed / 2;
            }
            else
            {
                _maxFreezeTime = 25;
                _speed = _minSpeed;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
