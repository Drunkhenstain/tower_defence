﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Game1
{
    public abstract class Level
    {
        public int CurrentWave;
        protected Dictionary<string, Creep> _deadCreeps = new Dictionary<string, Creep>();

        public LinkedList<Creep> _creeps = new LinkedList<Creep>();
        public LinkedList<Tower> _towers = new LinkedList<Tower>();
        public LinkedList<TowerPlatform> _platforms = new LinkedList<TowerPlatform>();
        public LinkedList<TowerPlatform> _mountedPlatforms = new LinkedList<TowerPlatform>();

        public int CoinsSpent;
        public Texture2D _background;

        protected SvgDataContainer _levelLogic;

        public int Id { get; private set; }

        protected int _defaultEnemyAmount;
        protected int _enemyAmount;
        protected int _projectileCount;
        protected int _startDelay;

        public int Hardness;
        public int PlayerCoins;
        public int Failures;

        protected bool _frameIsEven;
        protected bool _isActive;


        public Level(int id)
        {
            Id = id;
        }

        public virtual void Initialize(int levelId)
        {
            _frameIsEven = false;
            _isActive = false;
            Id = levelId;
        }

        public virtual void Reset()
        {
            CurrentWave = 1;
            _enemyAmount = _defaultEnemyAmount;
            Failures = 0;
            _creeps = new LinkedList<Creep>();
            _towers = new LinkedList<Tower>();
            _platforms = new LinkedList<TowerPlatform>();
            _deadCreeps = new Dictionary<string, Creep>();
        }

        private void checkForRange(Tower tower, Creep creep)
        {
            if (tower != null && creep != null)
            {
                if (CollisionHelper.BoundingCircle((int)tower.colX(), (int)tower.colY(), tower.RangeInPx,
                                                    (int)creep.DrawingCenter().X, (int)creep.DrawingCenter().Y, creep.drawingSquare().Width / 2))
                {

                    if (tower.HasMaxTargets() == false && creep.isActive() == true)
                        tower.AddTarget(creep);
                }
                else
                {
                    tower.RemoveTarget(creep);
                }
            }
        }

        public virtual void GetInput()
        {

        }

        public virtual void Update(GameTime gameTime)
        {
            _projectileCount = 0;
            _deadCreeps.Clear();
            /// check if towers are on map
            /// 

            int lostLives = 0;
            int collectedCoins = 0;
            if (_towers.Count > 0)
            {
                /// when startTimer is over
                /// update all active creeps only if
                /// start timer is over
                /// 
                if (GameManager.UserTower != null)
                {
                    GameManager.UserTower.Update(gameTime);

                }
                foreach (Creep creep in _creeps)
                {
                    creep.Update(gameTime);
                }

                foreach (Tower tower in _towers)
                {
                    /// update all active towers
                    ///
                    tower.Update(gameTime);

                    /// update projectile counter for debugging
                    /// 
                    _projectileCount += tower.ActiveProjectilesCount();

                    /// when startTimer is over
                    /// search for dead creeps, if found they are moved to
                    /// List for dead creeps and will be later removed
                    /// if not dead creeps will be checked for each towers
                    /// range...
                    /// 

                    /// DONT FORGET TO CHANGE NO TOWER CASE WHEN EDITING!!!!!!!!!!
                    /// 
                    foreach (Creep creep in _creeps)
                    {
                        if (creep.IsDead() == true)
                        {
                            _deadCreeps.Add(creep.Id(), creep);
                            if (creep.ReachedGoal == false)
                            {
                                int loot = creep.Loot();
                                collectedCoins += loot;
                                PlayerCoins += loot;
                            }
                            else
                            {
                                lostLives++;
                                Failures++;
                            }
                        }
                        else
                        {
                            if (tower.isActive() == true)
                            {
                                checkForRange(tower, creep);
                            }
                            checkForRange(GameManager.UserTower, creep);

                            if (creep._isStunned == false)
                            {
                                if (CollisionHelper.BoundingRectangle(GameManager.UserTower.drawingSquare(), creep.drawingSquare()))
                                {
                                    GameManager.UserTower.StunMe();
                                }
                            }
                        }
                    }

                    /// remove dead creeps from creep list and from
                    /// each towers target list
                    /// 
                    foreach (Creep deadCreep in _deadCreeps.Values)
                    {
                        if (GameManager.UserTower != null)
                        {
                            GameManager.UserTower.RemoveTarget(deadCreep);
                        }
                        if (tower.isActive() == true)
                        {
                            tower.RemoveTarget(deadCreep);
                        }
                        _creeps.Remove(deadCreep);
                    }
                }
            }
            else
            {
                if (GameManager.UserTower != null)
                {
                    GameManager.UserTower.Update(gameTime);
                }
                foreach (Creep creep in _creeps)
                {
                    creep.Update(gameTime);

                    if (GameManager.UserTower != null)
                    {
                        if (CollisionHelper.BoundingRectangle(GameManager.UserTower.drawingSquare(), creep.drawingSquare()))
                        {
                            if (creep._isStunned == false)
                            {
                                GameManager.UserTower.StunMe();
                            }
                        }
                        checkForRange(GameManager.UserTower, creep);
                        if (creep.IsDead() == true)
                        {
                            _deadCreeps.Add(creep.Id(), creep);
                            if (creep.ReachedGoal == false)
                            {
                                int loot = creep.Loot();
                                PlayerCoins += loot;
                                collectedCoins += loot;
                            }
                            else
                            {
                                lostLives++;
                                Failures++;
                            }
                        }
                    }

                }
                foreach (Creep deadCreep in _deadCreeps.Values)
                {
                    if (GameManager.UserTower != null)
                    {
                        GameManager.UserTower.RemoveTarget(deadCreep);
                    }
                    _creeps.Remove(deadCreep);
                }
            }

            _frameIsEven = !_frameIsEven;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (Creep creep in _creeps)
            {
                creep.Draw(spriteBatch);
            }
            foreach (Tower tower in _towers)
            {
                tower.Draw(spriteBatch);
            }
            foreach (TowerPlatform platform in _platforms)
            {
                platform.draw(spriteBatch);
            }
            HudManager.DrawBackgrounds(spriteBatch);
            if (GameManager.UserTower != null)
            {
                GameManager.UserTower.Draw(spriteBatch);
            }
        }

        public bool DoSplashDamage(Projectile projectile, int maxTargets)
        {
            int i = 1;
            foreach (Creep creep in _creeps)
            {
                if (CollisionHelper.BoundingCircle((int)projectile.colX(), (int)projectile.colY(), projectile.SplashRange(), (int)creep.colX(), (int)creep.colY(), creep.colWidth()))
                    creep.GetDamage(projectile);

                if (i == maxTargets)
                {
                    break;
                }
                i++;
            }
            return true;
        }

        public virtual void SpawnWave()
        {
        }

        public bool BuyTower(Tower tower)////!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        {
            return true;
            //if (PlayerCoins - tower.UpgradeCost >= 0)
            //{
            //    PlayerCoins -= (int)tower.UpgradeCost;
            //    CoinsSpent += (int)tower.UpgradeCost;
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        public int EnemyCount()
        {
            return _creeps.Count;
        }

        public int ProjectileCount()
        {
            return _projectileCount;
        }

        public int Wave()
        {
            return CurrentWave;
        }
    }
}
