﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    public class TestLevel : Level
    {

        public int WaveAmount;
        private int _difficulty;
        private int _maxEnemyStartDelay;
        private int _maxEnemyGroupSize;

        private int _freezeTwCount;
        private int _basicTwCount;
        private int _splashTwCount;

        public TestLevel(int id, int logicId, int difficulty, int enemyAmount, Texture2D background) : base(id)
        {
            _difficulty = difficulty;
            Hardness = difficulty;
            _background = background;
            _levelLogic = MySvgReadAndParser.GetDataWithDescription("test" + logicId);
            _defaultEnemyAmount = enemyAmount;
            _enemyAmount = enemyAmount;

            _startDelay = 400;
            _maxEnemyStartDelay = 2000;
            _maxEnemyGroupSize = 1;
            WaveAmount = 15;
            CurrentWave = 1;
        }

        public override void Initialize(int levelId)
        {
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Level " + Id + " started", Color.Green);

            base.Initialize(levelId);
            PlayerCoins = CalcPlayerPoints();

            SpawnWave();
            //SpawnAllTowers();           //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            SpawnTowerPlatforms();
        }

        public override void SpawnWave()
        {
            float _amountifier = 1;

            if (CurrentWave < 5)
            {
                _amountifier += 0.45f;
            }
            else if (CurrentWave < 7)
            {
                _amountifier += 0.38f;
            }
            else if (CurrentWave < 10)
            {
                _amountifier += 0.27f;
            }
            else if (CurrentWave < 14)
            {
                _amountifier += 0.19f;
            }
            else
            {
                _amountifier += 0.14f;
            }


            _enemyAmount = (int)(_enemyAmount * _amountifier);


            if (GameManager._lvlsPlayedInRow > 0)
            {
                _enemyAmount += (GameManager._lvlsPlayedInRow - 1) * 3;
            }

            if (CurrentWave <= WaveAmount)
            {
                //DebugHelper.AddLog(MyMathHelper.GetGuid(), "<---------------------->", Color.White);
                DebugHelper.AddLog(MyMathHelper.GetGuid(), "Wave: " + CurrentWave + " Spawned", Color.Red);
                DebugHelper.AddLog(MyMathHelper.GetGuid(), "Damn! " + _enemyAmount + " Creeps Incoming", Color.White);
                CurrentWave++;
                SpawnEnemys();
            }

        }

        private void SpawnEnemys()
        {
            LinkedList<Creep> creepHeap = new LinkedList<Creep>();

            _maxEnemyStartDelay = (CurrentWave * 1600) + 1100;
            _maxEnemyGroupSize = 2 + (int)(CurrentWave / 2.1);

            if (_maxEnemyStartDelay > 15000)
            {
                _maxEnemyStartDelay = 15000;
            }

            int countFat = 0;
            int countFast = 0;
            int countBasic = 0;

            bool baxaSpawned = false;

            for (int i = 0; ; i++)
            {
                int extraFatCreeps = 0;
                int extraBasicCreeps = 0;
                int extraFastCreeps = 0;

                if (CurrentWave > 4)
                {
                    if (_splashTwCount < _basicTwCount && _splashTwCount < _freezeTwCount)
                    {
                        extraBasicCreeps = 4;
                    }

                    if (_basicTwCount < _splashTwCount && _basicTwCount < _freezeTwCount)
                    {
                        extraFatCreeps = 1;
                    }

                    if (_freezeTwCount < _splashTwCount && _freezeTwCount < _basicTwCount)
                    {
                        extraFastCreeps = 4;
                    }
                }

                int rndLane = MyMathHelper.GetRandom(0, _levelLogic.Paths.Count - 1);
                if (i < (_enemyAmount / 100f) * 5f + extraFatCreeps - extraBasicCreeps)
                {
                    if (CurrentWave > 6)
                    {
                        creepHeap.AddFirst(new FatCreep(_levelLogic.Paths[rndLane].polylines[0]  //select random path to walk
                            , FontSpriteManager.creep1, "fat" + MyMathHelper.GetGuid(), 0, rndLane));
                        countFat++;
                    }
                }
                else if (i < (_enemyAmount / 100f) * 30f + extraFastCreeps - extraBasicCreeps)
                {
                    creepHeap.AddFirst(new FastCreep(_levelLogic.Paths[rndLane].polylines[0]  //select random path to walk
                        , FontSpriteManager.bullet, "fast" + MyMathHelper.GetGuid(), 0, rndLane));
                    countFast++;

                }
                else
                {
                    creepHeap.AddFirst(new BasicCreep(_levelLogic.Paths[rndLane].polylines[0]  //select random path to walk
                        , FontSpriteManager.GetRandomShip(), "basic" + MyMathHelper.GetGuid(), 0, rndLane));
                    countBasic++;

                }
                if (CurrentWave > 9 && baxaSpawned == false)
                {
                    creepHeap.AddFirst(new UglyCreep(_levelLogic.Paths[2].polylines[0]
                                   , FontSpriteManager.uglybaxa, "ugly" + MyMathHelper.GetGuid(), 0, rndLane));
                    baxaSpawned = true;
                }
                if (creepHeap.Count >= _enemyAmount)
                {
                    break;
                }
            }

            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Standard Creeps: " + countBasic, Color.White);
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Fat Creeps: " + countFat, Color.White);
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Fast Creeps: " + countFast, Color.White);

            bool tempEven = false;
            float groupDelay = _startDelay;
            int groupAmount = _maxEnemyGroupSize;
            int groupCounter = 0;

            float extraSpeed = (CurrentWave - 2) * 1.3f;
            float extraLive = ((CurrentWave - 2) * 90f) + (CurrentWave * 40);
            float extraArmor = (CurrentWave - 2) * 3f;

            int countCreeps = 0;
            foreach (Creep creep in creepHeap)
            {
                /// let fat creeps spawn in the second half of wave
                /// 
                if (creep.Id().Contains("fat"))
                {
                    creep.Initialize(_levelLogic, tempEven, Hardness, _maxEnemyStartDelay - MyMathHelper.GetRandom(_maxEnemyStartDelay / 4, _maxEnemyStartDelay / 2));
                }
                else if (creep.Id().Contains("ugly"))
                {
                    creep.Initialize(_levelLogic, tempEven, Hardness, _maxEnemyStartDelay - _maxEnemyStartDelay / 5);
                }
                else
                {
                    creep.Initialize(_levelLogic, tempEven, Hardness, (int)groupDelay);
                }
                creep.UpgradeCreep(extraSpeed, extraLive, extraArmor, CurrentWave);
                tempEven = !tempEven;
                if (groupCounter >= groupAmount)
                {
                    //groupDelay = MyMathHelper.GetRandom(_startDelay, _startDelay + MyMathHelper.GetRandom(_startDelay, _maxEnemyStartDelay));
                    groupDelay = ((float)countCreeps / _enemyAmount) * _maxEnemyStartDelay;
                    groupAmount = _maxEnemyGroupSize;
                    groupCounter = 0;
                }
                countCreeps++;
                groupCounter++;
            }
            foreach (Creep creep in creepHeap)
            {
                _creeps.AddFirst(creep);
            }

            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Extra Speed:  " + (int)extraSpeed, Color.White);
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Extra Live:   " + (int)extraLive, Color.White);
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Extra Shield: " + (int)extraArmor, Color.White);
        }

        private void SpawnAllTowers()
        {
            foreach (Vector2 towerPos in _levelLogic.Circles)
            {
                int rnd = MyMathHelper.GetRandom(1, 100);
                Vector2 offset = new Vector2(-25, -25);
                if (rnd < 33)
                {
                _towers.AddFirst(new SplashTower(towerPos + offset, FontSpriteManager.asteroid, "splash" + MyMathHelper.GetGuid(), 1, this));
                }
                else if (rnd<66)
                {
                    _towers.AddFirst(new BasicTower(towerPos + offset, FontSpriteManager.asteroid, "basic" + MyMathHelper.GetGuid(), 1, this));

                }
                else
                {
                    _towers.AddFirst(new FreezeTower(towerPos + offset, FontSpriteManager.asteroid, "freeze" + MyMathHelper.GetGuid(), 1, this));

                }
            }
            bool tempEven = false;
            foreach (Tower tower in _towers)
            {
                tower.Initialize(tempEven);
                tempEven = !tempEven;
            }
        }

        private void SpawnTower(Tower tower, bool updateEvenFrame)
        {
            switch (tower.Type)
            {
                case "freeze":
                    _freezeTwCount++;
                    break;

                case "splash":
                    _splashTwCount++;
                    break;

                case "basic":
                    _basicTwCount++;
                    break;
            }

            tower.Initialize(updateEvenFrame);
            _towers.AddFirst(tower);
        }

        private void SpawnTowerPlatforms()
        {
            int ti = 0;
            foreach (Vector2 towerPos in _levelLogic.Circles)
            {
                Vector2 offset = new Vector2(-25, -25);
                _platforms.AddFirst(new TowerPlatform(towerPos + offset, FontSpriteManager.asteroid, "platform" + MyMathHelper.GetGuid(), 1, this));
                ti++;
            }


            foreach (TowerPlatform platform in _platforms)
            {
                platform.Initialize();
            }
        }

        private int CalcPlayerPoints()
        {
            return (int)(((Math.E) * 1.6f) * 15);
        }

        public override void Update(GameTime gameTime)
        {
            if (GameManager.UserTower.position() == new Vector2(-50, -50))
            {
                GameManager.UserTower.setPosition(_levelLogic.Rectangles[0]);
                GameManager.UserTower.SetNextLevelStage(this);
            }

            GetInput();
            UpdatePlatforms();
            base.Update(gameTime);
        }

        private void UpdatePlatforms()
        {
            if (_platforms.Count > 0)
            {
                foreach (TowerPlatform platform in _platforms)
                {
                    Tower temp = platform.update();

                    if (temp != null)
                    {
                        SpawnTower(temp, true);
                    }

                    if (platform.IsMounted() == true)
                    {
                        _mountedPlatforms.AddFirst(platform);
                    }
                }

                foreach (TowerPlatform platform in _mountedPlatforms)
                {
                    _platforms.Remove(platform);
                }

                _mountedPlatforms.Clear();
            }
        }


        public override void Reset()
        {
            _freezeTwCount = 0;
            _splashTwCount = 0;
            _basicTwCount = 0;

            PlayerCoins = CalcPlayerPoints();
            base.Reset();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            //DrawEnemyPaths(spriteBatch);
        }

        private void DrawEnemyPaths(SpriteBatch spriteBatch)
        {
            foreach (Polyline line in _levelLogic.Paths)
            {
                Vector2 lastWaypoint = Vector2.Zero;
                foreach (Vector2 waypoint in line.polylines)
                {
                    if (lastWaypoint != Vector2.Zero)
                        DrawHelper.DrawFullLine(spriteBatch, lastWaypoint, waypoint, 1, Color.Red);

                    lastWaypoint = waypoint;
                }
            }
        }
    }
}
