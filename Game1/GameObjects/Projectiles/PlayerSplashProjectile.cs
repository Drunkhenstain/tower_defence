﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class PlayerSplashProjectile : Projectile
    {
        private TestLevel _level;
        private LightningEmitter _emitter;
        private int _maxTargets;


        public PlayerSplashProjectile(Vector2 position, Texture2D sprite, string id, int actorType, TestLevel level) : base(position, sprite, id, actorType)
        {
            _level = level;
            _maskColor = Color.Red;
            _drawingSquare.Size = new Point(26, 26);

            _speed = 80;
            _maxSpeed = 10000;
            _acceleration = MathHelper.E * 10;

            _maxTargets = 1000;
            _impactRange = 13;
            _splashRange = 22;
            _exploSize = new Vector2(90, 90);

            _maxLifespan = 50;
            _maxExploLifespan = 26;
            _damage = MyMathHelper.GetRandom(40, 65);

            _emitter = new LightningEmitter(_position,0.14f);

            _exploAnimSprite = FontSpriteManager.explosion5x5;

            _spriteRows = 5;
            _spriteCols = 5;
            _animator = new AnimatedSprite(_exploAnimSprite, _spriteRows, _spriteCols);
        }

        protected override void ChangeToAnim()
        {
            _drawingSquare.Size = new Point(60, 60);
            base.ChangeToAnim();
        }

        public override void Intitialize(Creep target)
        {
            _target = target;
            _targetPos = target.position();
            base.Intitialize(target);
        }

        public override void Hit()
        {
            _madeDamage = true;
            _level.DoSplashDamage(this, _maxTargets);
        }

        public override void Update(GameTime gameTime)
        {
            _emitter.Update(_position,_maskColor);

                _emitter.AddTarget(GameManager.UserTower.DrawingCenter());

            if (_currentLifespan - _lastColorChange > 2)
            {
                if (_maskColor == Color.LawnGreen)
                {
                    _maskColor = Color.MonoGameOrange;
                }
                else
                {
                    _maskColor = Color.LawnGreen;
                }
                _lastColorChange = _currentLifespan;
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_cuExploLifespan <= 0)
            {
                _emitter.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);
        }
    }
}



