﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{


    public abstract class Projectile : GameActor
    {
        protected bool _killMe;
        protected bool _atTarget;
        protected bool _updateTarget;
        protected bool _madeDamage;

        public float _damage;
        protected int _impactRange;

        public int EffectDuration;

        protected int _lastColorChange = 0;

        protected int _maxExploLifespan;
        protected int _cuExploLifespan;
        protected int _currentLifespan;
        protected int _maxLifespan;
        protected int _splashRange;

        protected float _distance;
        protected float _acceleration;
        protected float _maxSpeed;

        protected Creep _target;

        protected Color _exploColor;
        protected Texture2D _exploAnimSprite;

        protected Vector2 _targetPos;
        protected Vector2 _source;


        public Projectile(Vector2 position, Texture2D sprite, string id, int actorType) : base(position, sprite, id, actorType)
        {
            _source = position;
            _position = position;
            _updateTarget = false;
            _killMe = false;
            _madeDamage = false;

            _angleOrigin.X = _drawingSquare.Width / 2;
            _angleOrigin.Y = _drawingSquare.Height / 2;
        }

        public void SetColor(Color color)
        {
            _maskColor = color;
        }

        public virtual void Intitialize(Creep target)
        {
            _distance = MyMathHelper.CalcDistance(_source, _targetPos);
            _direction = MyMathHelper.CalcDirection(_source, _targetPos, _distance);
        }

        public virtual void SetDamage(int damage)
        {
            _damage = damage;
        }

        public virtual void RaiseDamage(float factor)
        {
            _damage *= factor;
        }

        public override void Update(GameTime gameTime)
        {
            _currentLifespan++;

            if (_speed + _acceleration < _maxSpeed)
            {
                _speed = _speed + _acceleration;
            }

            _angle = MyMathHelper.GetAngle(_position, _targetPos);

            if (_killMe == false)
            {
                if (_atTarget == false && MyMathHelper.CalcDistance(_targetPos, _position) <= _impactRange)
                {
                    _atTarget = true;
                    ChangeToAnim();
                }
                if (_currentLifespan > _maxLifespan + _maxExploLifespan || _target.Health() <= 0)
                {
                    _killMe = true;
                }
                if (_atTarget == false)
                {
                    _position = MyMathHelper.CalcSmoothMovement(_position, _direction, _speed, (float)gameTime.ElapsedGameTime.TotalSeconds);
                }
            }
            if (_atTarget == true)
            {
                if (_madeDamage == false)
                {
                    Hit();
                }
                _cuExploLifespan++;
            }
            if (_madeDamage == true && _cuExploLifespan >= _maxExploLifespan)
            {
                _killMe = true;
            }
            base.Update(gameTime);
        }

        protected virtual void ChangeToAnim()
        {
            _drawingSquare.X -= _drawingSquare.Size.X / 2;
            _drawingSquare.Y -= _drawingSquare.Size.Y / 2;
            _actorAnimType = (int)act_AnimType.STATIC;

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_cuExploLifespan > 0)
            {
                _drawingSquare.Location = (_targetPos - _exploSize / 2).ToPoint();
                _drawingSquare.Size = _exploSize.ToPoint();
            }

            if (_angle != 0)
            {
                base.Draw(spriteBatch);
            }

            if (_cuExploLifespan > 0)
            {
                DrawHelper.DrawExtraSmallText(spriteBatch, _damage.ToString(), _drawingSquare.Center.ToVector2(), 1, 0f, Color.White);
            }
        }

        public virtual void UpdateTarget()
        {
            _targetPos = _target.position();
            _distance = MyMathHelper.CalcDistance(_position, _targetPos);
            _direction = MyMathHelper.CalcDirection(_position, _targetPos, _distance);
        }

        public virtual void Hit()
        {
            _madeDamage = true;
            _target.GetDamage(this);
        }

        public int Damage()
        {
            return (int)_damage;
        }

        public bool IsDead()
        {
            return _killMe;
        }

        public int SplashRange()
        {
            return _splashRange;
        }
    }
}
