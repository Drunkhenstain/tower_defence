﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public class SingleTargetProjectile : Projectile
    {
        private SingleTargetEmitter _emitter;

        public SingleTargetProjectile(Vector2 position, Texture2D sprite, string id, int actorType) : base(position, sprite, id, actorType)
        {
            _maskColor = Color.Red;
            _exploColor = Color.Red;
            _drawingSquare.Size = new Point(25, 15);

            _speed = 1f;
            _maxSpeed = 3000;
            _acceleration = MathHelper.E * 7;
            _maxLifespan = 70;
            _maxExploLifespan = 17;

            _damage = 0;
            _impactRange = 10;
            _exploSize = new Vector2(25, 25);
            _animScale = 2;

            _exploAnimSprite = FontSpriteManager.explosion5x5;

            _spriteRows = 5;
            _spriteCols = 5;
            _animator = new AnimatedSprite(_exploAnimSprite, _spriteRows, _spriteCols);

            List<Texture2D> tempSprites = new List<Texture2D>();
            tempSprites.Add(FontSpriteManager.circle);
            tempSprites.Add(FontSpriteManager.triangle);
            _emitter = new SingleTargetEmitter(tempSprites, position);
        }

        public override void Intitialize(Creep target)
        {
            _target = target;
            _targetPos = target.DrawingCenter();


            base.Intitialize(target);
        }

        public override void Update(GameTime gameTime)
        {
            if (_currentLifespan - _lastColorChange > 2)
            {
                if (_maskColor == Color.LawnGreen)
                {
                    _maskColor = Color.Red;
                }
                else
                {
                    _maskColor = Color.LawnGreen;
                }
                _lastColorChange = _currentLifespan;
            }
            UpdateTarget();
            if (Id() != "aoefreeze")
            {
                _emitter.Update(_position, _maskColor);
            }
            base.Update(gameTime);
        }

        protected override void ChangeToAnim()
        {
            _drawingSquare.Size = new Point(30, 30);
            base.ChangeToAnim();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_cuExploLifespan <= 0)
            {
                _emitter.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);
        }
    }
}
