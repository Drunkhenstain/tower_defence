﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class SplashProjectile : Projectile
    {
        private TestLevel _level;
        private SplashEmitter _emitter;
        private int _maxTargets;

        public SplashProjectile(Vector2 position, Texture2D sprite, string id, int actorType, TestLevel level) : base(position, sprite, id, actorType)
        {
            _level = level;
            _maskColor = Color.HotPink;
            _drawingSquare.Size = new Point(26, 26);

            _speed = 7;
            _maxSpeed = 1000;
            _acceleration = MathHelper.E * 3;

            _maxTargets = 1000;
            _impactRange = 10;
            _splashRange = 19;
            _exploSize = new Vector2(90, 90);

            _maxLifespan = 40;
            _maxExploLifespan = 26;
            _damage = MyMathHelper.GetRandom(40, 65);

            List<Texture2D> tempSprites = new List<Texture2D>();
            tempSprites.Add(FontSpriteManager.circle);
            tempSprites.Add(FontSpriteManager.triangle);
            _emitter = new SplashEmitter(tempSprites, position);

            _exploAnimSprite = FontSpriteManager.explosion5x5;

            _spriteRows = 5;
            _spriteCols = 5;
            _animator = new AnimatedSprite(_exploAnimSprite, _spriteRows, _spriteCols);
        }

        protected override void ChangeToAnim()
        {
            _drawingSquare.Size = new Point(60, 60);
            base.ChangeToAnim();
        }

        public override void Intitialize(Creep target)
        {
            _target = target;
            _targetPos = target.position();
            base.Intitialize(target);

        }

        public override void Hit()
        {
            _madeDamage = true;
            _level.DoSplashDamage(this, _maxTargets);
        }

        public override void Update(GameTime gameTime)
        {

            _emitter.Update(_position, _direction, _maskColor);

            if (_currentLifespan - _lastColorChange > 2)
            {
                if (_maskColor == Color.OrangeRed)
                {
                    _maskColor = Color.Yellow;
                }
                else
                {
                    _maskColor = Color.OrangeRed;
                }
                _lastColorChange = _currentLifespan;
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_cuExploLifespan <= 0)
            {
                _emitter.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);
        }
    }
}
