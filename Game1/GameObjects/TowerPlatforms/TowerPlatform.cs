﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace Game1
{
    public class TowerPlatform
    {
        Vector2 _position;
        Vector2 _size;
        Color _maskColor;
        protected TestLevel _level;
        public string Id;

        private bool _isMounted;
        private string _playerChoice;

        public TowerPlatform(Vector2 position, Texture2D sprite, string id, int actorType, TestLevel level)
        {
            Id = id;
            _level = level;
            _position = position;
            _size = new Vector2(55, 50);
            _maskColor = Color.Transparent;
        }

        public void Initialize()
        {
            HudManager.AddBackgroundButton(Id + "standard", new Button(_position + new Vector2(0, (_size.Y / 3)*-1), new Vector2(_size.X, _size.Y/3), "extrasmall hold", "basic17", 30, new Color(170, 57, 57)));
            HudManager.AddBackgroundButton(Id + "splash", new Button(_position + new Vector2(0, 0), new Vector2(_size.X, _size.Y / 3), "extrasmall hold", "multi18", 30, new Color(173, 255, 164)));
            HudManager.AddBackgroundButton(Id + "freeze", new Button(_position + new Vector2(0, _size.Y / 3), new Vector2(_size.X, _size.Y / 3), "extrasmall hold", "stun23", 30, new Color(118, 184, 184)));
        }

        private void getInput()
        {
            _playerChoice = "none";
            if (_isMounted == false)
            {
                if (HudManager.GetButtonPress(Id + "standard"))
                {
                    if (_level.BuyTower(new BasicTower(_position, FontSpriteManager.asteroid, "standardTower", 1, _level)))
                    {
                        _playerChoice = "standard";
                        _isMounted = true;

                        HudManager.RemoveBackgroundButton(Id + "freeze");
                        HudManager.RemoveBackgroundButton(Id + "splash");
                        HudManager.RemoveBackgroundButton(Id + "standard");
                    }
                }
                if (HudManager.GetButtonPress(Id + "splash"))
                {
                    if (_level.BuyTower(new SplashTower(_position, FontSpriteManager.asteroid, "splash", 1, _level)))
                    {
                        _playerChoice = "splash";
                        _isMounted = true;

                        HudManager.RemoveBackgroundButton(Id + "freeze");
                        HudManager.RemoveBackgroundButton(Id + "splash");
                        HudManager.RemoveBackgroundButton(Id + "standard");
                    }
                }
                if (HudManager.GetButtonPress(Id + "freeze"))
                {
                    if (_level.BuyTower(new FreezeTower(_position, FontSpriteManager.asteroid, "freeze", 1, _level)))
                    {
                        _playerChoice = "freeze";
                        _isMounted = true;

                        HudManager.RemoveBackgroundButton(Id + "freeze");
                        HudManager.RemoveBackgroundButton(Id + "splash");
                        HudManager.RemoveBackgroundButton(Id + "standard");
                    }
                }
            }
        }

        public Tower update()
        {


            getInput();
            switch (_playerChoice)
            {
                case "none":
                    return null;

                case "splash":
                    _isMounted = true;
                    DebugHelper.AddLog("temp"+Id, "Splash Tower Installed", Color.Blue);
                    return new SplashTower(_position, FontSpriteManager.asteroid, "splash" + MyMathHelper.GetGuid(), 1, _level);

                case "standard":
                    _isMounted = true;
                    DebugHelper.AddLog("temp" + Id, "Basic Tower Installed", Color.Blue);
                    return new BasicTower(_position, FontSpriteManager.asteroid, "basic" + MyMathHelper.GetGuid(), 1, _level);

                case "freeze":
                    _isMounted = true;
                    DebugHelper.AddLog("temp" + Id, "Freeze Tower Installed", Color.Blue);
                    return new FreezeTower(_position, FontSpriteManager.asteroid, "freeze" + MyMathHelper.GetGuid(), 1, _level);

                default:
                    return null;
            }
        }

        public bool IsMounted()
        {
            return _isMounted;
        }

        public void draw(SpriteBatch spriteBatch)
        {
            //DrawHelper.DrawFilledRectangle(spriteBatch, _position, _size, 0, _maskColor);
            //DrawHelper.DrawEmptyRectangle(spriteBatch, _position, _size, 3, Color.Black);
        }
    }
}

