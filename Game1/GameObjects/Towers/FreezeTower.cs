﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    public class FreezeTower : Tower
    {
        public FreezeTower(Vector2 position, Texture2D sprite, string id, int actorType, TestLevel level) : base(position, sprite, id, actorType, level)
        {
            _timeForUpgrade = 1500;
            //_maxTwrLvl = 8;
            _autoFireRate = 1100;
            RangeInPx = 150;

            _maxTargets = 3;
            EffectDuration = 220;
            _singleTargetDmgPerShot = 10;

            _drawingSquare.Size = new Point(50);
            _maskColor = new Color(118, 184, 184);
            _critChance = 0;
            _critMultiplier = (float)(Math.E / 2);
            UpgradeCost = 23;
            Type = "freeze";
        }

        public override void Initialize(bool updateEvenFrame)
        {
            base.Initialize(updateEvenFrame);
        }

        public override void Upgrade()
        {
            UpgradeCost *= (float)Math.E;

            _timeForUpgrade *= 2;

            _singleTargetDmgPerShot += 2;
            EffectDuration += 30;

            _critMultiplier *= 1.4f;

            if (_autoFireRate - 150 > 300)
            {
                _autoFireRate -= 100;
            }

            if (_maxTargets < 10)
            {
                _maxTargets += 1;
            }

            RangeInPx += 3;
            switch (_towerLvl)
            {
                case 1:
                    _maskColor = new Color(118, 184, 184);

                    break;

                case 2:
                    _maskColor = new Color(69, 147, 147);
                    break;

                case 3:

                    _maskColor = new Color(43, 126, 126);
                    break;

                case 4:

                    _maskColor = new Color(23, 108, 108);
                    break;

                case 5:
                    _maskColor = new Color(8, 86, 86);
                    _autoFireRate -= 70;
                    break;

                case 6:
                    _maskColor = Color.BlueViolet;
                    break;
            }
            base.Upgrade();
            
        }



        public override void getInput()
        {

            base.getInput();

        }

        public override void Update(GameTime gameTime)
        {
            getInput();
            if (_isActive == true)
            {
                base.Update(gameTime);
                LinkedList<string> tempIds = _lastTargetIds;
                if (_defaultRldTimer.ElapsedMilliseconds >= _autoFireRate)
                {
                    foreach (Creep target in _currentTargets)
                    {
                        if (tempIds.Contains(target.Id()) == false)
                        {
                            ShootProjectile(target, new SingleTargetProjectile(DrawingCenter(), FontSpriteManager.circle, "singlefreeze", (int)act_Type.PROJECTILE));
                            _lastTargetIds.AddFirst(target.Id());
                            _lastTarget = target;
                            _defaultRldTimer.Restart();
                            break;
                        }
                        if (_lastTargetIds.Count >= _currentTargets.Count)
                        {
                            _lastTargetIds.Clear();
                        }
                    }
                    _shotThisFrame = true;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            base.Draw(spriteBatch);

        }
    }
}
