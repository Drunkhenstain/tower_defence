﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Game1
{
    abstract public class GameActor
    {
        protected Color _defaultColor;

        protected Color _maskColor;

        protected List<string> _stateHistory = new List<string>();

        protected Vector2 _position;
        protected Vector2 _colPosition;
        protected Vector2 _oldPosition;
        protected Vector2 _tempPosition;
        protected Vector2 _direction;
        protected Vector2 _exploSize;
        protected Vector2 _angleOrigin;
        protected Vector2 _deceleration;

        protected Texture2D _sprite;

        protected AnimatedSprite _animator;

        protected SpriteEffect _spriteEffectType;

        protected Rectangle _colissionSquare;
        protected Rectangle _drawingSquare;

        protected bool _walkable;
        protected bool _isActive;
        protected bool _updateEvenFrame;
        protected bool _mouseIsOver;

        protected string _actorId;
        protected string _name;
        protected string _group;
        protected string _lastState;
        protected string _spriteDirection;

        protected float _decelerationFactor;
        protected float _angle;
        protected float _updateDelay;
        protected float _zLayer;
        protected float _spriteFrameDelay;
        protected float _speed;
        protected float _standardAnimDelay;
        protected float _animDelay;
        protected float _tempAnimDelay;
        protected float _animScale;

        protected int _userInput;
        protected int _lastUpdateTime;
        protected int _currentState;
        protected int _spriteCols;
        protected int _spriteRows;
        protected int _actorAnimType;
        protected int _colissionXmodifier;
        protected int _colissionYmodifier;
        protected int _colissionWidth;
        protected int _colissionHeigt;
        protected int _actorType;
        protected int _spriteNumFrames;
        protected int _animDirection;
        protected int _spriteRow;
        protected int _crashedInto;
        protected int _lastDirection;

        public GameActor(Vector2 position, Texture2D sprite, string id, int actorType)
        {
            /// Define essential actor stuff mostly for drawing

            _position = position;
            _sprite = sprite;
            _colissionSquare = new Rectangle(position.ToPoint(), new Point(sprite.Width, sprite.Height));
            _drawingSquare = new Rectangle(position.ToPoint(), new Point(sprite.Width, sprite.Height));
            _crashedInto = (int)act_crashedInto.NONE;
            _maskColor = Color.White;
            _actorId = id;
            _actorType = actorType;
            _angleOrigin = new Vector2(0, 0);
            _spriteDirection = "";
            _mouseIsOver = false;
        }


        public virtual void Update(GameTime gameTime)
        {
            updateAnim();
            /// update rectangles for drawing and gamelogic
            /// 
            if (_actorAnimType == (int)act_AnimType.NONE)
            {
                _drawingSquare.X = (int)_position.X;
                _drawingSquare.Y = (int)_position.Y;
            }
            _colissionSquare.X = _drawingSquare.X + _colissionXmodifier;
            _colissionSquare.Y = _drawingSquare.Y + _colissionYmodifier;


            /// calculate originVector from sprite for drawing rotated sprites
            /// 
            
            /// update oldPosition and animation if needed

            _oldPosition = _position;
        }

        public virtual void updateAnim()
        {
            /// Calculate actor direction based on current and last position
            /// needs to be fixed
            ///

            float dirX = _position.X - _oldPosition.X;
            float dirY = _position.Y - _oldPosition.Y;
            if (dirX > 0 && dirY > 0)
            {
                if (dirX >= dirY)
                {
                    _spriteDirection = act_Direction.RIGHT.ToString();   /// Moch des gscheid
                }
                else
                {
                    _spriteDirection = act_Direction.DOWN.ToString();
                }
            }
            else
            {
                if (dirX >= 0)
                {
                    if (dirY * -1 > dirX)
                    {
                        _spriteDirection = act_Direction.UP.ToString();
                    }
                }
                if (dirY >= 0)
                {
                    if (dirX * -1 > dirY)
                    {
                        _spriteDirection = act_Direction.LEFT.ToString();
                    }
                }
            }

            /// update animator if needed

            if (_actorAnimType != (int)act_AnimType.NONE)
            {
                if (_actorAnimType == (int)act_AnimType.STATIC)
                    _animator.Update();
                if (_actorAnimType == (int)act_AnimType.DYNAMIC)
                    _animator.Update(_animDirection);
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            switch (_actorAnimType)
            {
                case (int)act_AnimType.NONE:
                    {
                        spriteBatch.Draw(_sprite, drawingSquare(), null, _maskColor, _angle, _angleOrigin, SpriteEffects.None, 0);
                        break;
                    }

                case (int)act_AnimType.STATIC:
                    {
                        _animator.Draw(spriteBatch, _drawingSquare, _animScale, _maskColor);
                        break;
                    }

                case (int)act_AnimType.DYNAMIC:
                    {
                        _animator.Draw(spriteBatch, _drawingSquare.Location.ToVector2(), _spriteRow, 1, _maskColor);
                        break;
                    }
            }
        }

        protected void AddHistory(string state)
        {
            if (_stateHistory.Count > 30)
                _stateHistory.RemoveAt(0);
            if (_lastState != state)
                _stateHistory.Add(state);
            else if (_stateHistory.Count == 0)
                _stateHistory.Add(state);

            _lastState = state;
        }

        public bool walkable()
        {
            return _walkable;
        }

        public bool isActive()
        {
            return _isActive;
        }

        public int actorType()
        {
            return _actorType;
        }

        public string actorName()
        {
            return _name;
        }

        public int itemState()
        {
            return _currentState;
        }

        public int crashedInto()
        {
            return _crashedInto;
        }

        public int colX()
        {
            return _colissionSquare.X;
        }

        public int colY()
        {
            return _colissionSquare.Y;
        }

        public int colWidth()
        {
            return _colissionSquare.Width;
        }

        public int colHeight()
        {
            return _colissionSquare.Height;
        }

        public string state()
        {
            return _lastState;
        }

        public bool UpdateEven()
        {
            return _updateEvenFrame;
        }

        public string Id()
        {
            return _actorId;
        }

        public Rectangle drawingSquare()
        {
            return _drawingSquare;
        }

        public Vector2 direction()
        {
            return _direction;
        }

        public Vector2 DrawingCenter()
        {
            return _drawingSquare.Center.ToVector2();
        }

        public Vector2 position()
        {
            return _position;
        }

        public Vector2 oldPosition()
        {
            return _oldPosition;
        }

        public Vector2 colPosition()
        {
            return new Vector2(_colissionSquare.X, _colissionSquare.Y);
        }

        public float speed()
        {
            return _speed;
        }

        public Color maskColor()
        {
            return _maskColor;
        }

        public void setMaskColor(Color color)
        {
            _maskColor = color;
        }

        public void setWalkable(bool walkable)
        {
            _walkable = walkable;
        }

        public void setPosition(Vector2 position)
        {
            _position = position;
        }

        public void setCrashedInto(int crashedInto)
        {
            _crashedInto = crashedInto;
        }

        public void setX(float posX)
        {
            _position.X = posX;
        }

        public void setY(float posY)
        {
            _position.Y = posY;
        }

        public void setColissionX(int xModifier)
        {
            _colissionXmodifier = xModifier;
        }

        public void setColissionY(int yModifier)
        {
            _colissionYmodifier = yModifier;
        }

        public void setColissionWidth(int width)
        {
            _colissionSquare.Width = width;
        }

        public void setColissionHeigth(int height)
        {
            _colissionSquare.Height = height;
        }

    }
}

