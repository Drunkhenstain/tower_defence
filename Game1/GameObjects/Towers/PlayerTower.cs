﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace Game1
{
    class PlayerTower : Tower
    {
        private Creep _userCannonTarget;

        private bool _doShootMain;

        private float _maxSpeed;

        private float _portRate;
        private float _timeSincePort;
        private bool _doPort;

        private float _aoeDamage;
        private float _aoeDuration;
        private float _maxAoeTargets;
        private float _aoeRate;
        private float _aoeRange;
        private float _timeSinceAoe;
        private bool _doAoe;

        private LightningEmitter _aoeEmitter;
        private ReachGoalEmitter _portEmitter;

        public PlayerTower(Vector2 position, Texture2D sprite, string id, int actorType, TestLevel level) : base(position, sprite, id, actorType, level)
        {
            _drawingSquare.Size = new Point(50, 50);
            _maskColor = Color.Silver;
            _defaultColor = _maskColor;
            _timeForUpgrade = 3000;
            _doShootMain = false;

            _level = level;

            _autoFireRate = 500;
            _splashFireRate = 1300;

            _portRate = 350;

            _maxTargets = 1;

            RangeInPx = 170;
            EffectDuration = 100;

            _timeSinceLastStun = 0;
            _timeSinceStun = 0;
            _maxStunTime = 400;

            _maxSpeed = 2;
            _speed = 0.02f;

            _singleTargetDmgPerShot = 80;

            _splashDmgPerShot = 200;
            _critChance = 0;
            _critMultiplier = (float)Math.E;

            _maxAoeTargets = 15;
            _aoeRate = 700;
            _timeSinceAoe = 0;
            _doAoe = false;
            _aoeDamage = 100;
            _aoeDuration = 400;
            _aoeRange = RangeInPx + 10;

            _deceleration = new Vector2(0, 0);
            _decelerationFactor = _speed / 0.9f;
            _aoeEmitter = new LightningEmitter(_position, 0.02f);

            List<Texture2D> tempSprites = new List<Texture2D>();
            tempSprites.Add(FontSpriteManager.rawWhite);
            tempSprites.Add(FontSpriteManager.circle);
            _portEmitter = new ReachGoalEmitter(tempSprites, DrawingCenter());

            UpgradeCost = 100;
            Type = "player";
        }

        public override void Upgrade()
        {
            UpgradeCost *= 2f;

            _timeForUpgrade *= 2;

            _singleTargetDmgPerShot *= (float)1.4f;

            _splashDmgPerShot *= 1.4f;

            _portRate -= 60;
            _aoeRate -= 90;

            _critMultiplier *= 1.4f;

            if (_splashFireRate - 50 > 50)
            {
                _splashFireRate -= 50;
            }

            if (_autoFireRate - 40 > 80)
            {
                _autoFireRate -= 40;
            }

            if (_maxTargets < 1)
            {
                _maxTargets += 1;
            }

            RangeInPx += 5;
            _aoeRange = RangeInPx + 10;
            _maxSpeed += 0.3f;
            _speed += 0.05f;
            _maxStunTime -= 50;

            _decelerationFactor = _speed / 1.5f;

            switch (_towerLvl)
            {
                case 1:
                    _maskColor = Color.Silver;
                    break;

                case 2:
                    _maskColor = Color.Gold;
                    _maxAoeTargets *= 2;
                    break;

                case 3:
                    _maskColor = Color.Goldenrod;
                    _autoFireRate -= 40;
                    _splashFireRate -= 50;
                    _singleTargetDmgPerShot += 10;
                    _splashDmgPerShot += 5;
                    _maxAoeTargets *= 2;
                    break;

                case 4:
                    _maxAoeTargets *= 2;
                    _maskColor = Color.DarkGoldenrod;
                    break;

            }

            base.Upgrade();
        }

        public override void SetNextLevelStage(TestLevel level)
        {
            _maxTwrLvl = GameManager._lvlsPlayedInRow + 3;
            base.SetNextLevelStage(level);
        }

        public override void Initialize(bool isEven)
        {
            base.Initialize(isEven);

            _doPort = false;
            _timeSincePort = 0;
            _autoRldTimer = new Stopwatch();
            _autoRldTimer.Start();
        }

        public override void getInput()
        {
            base.getInput();
            bool move = false;

            _oldPosition = _position;
            ProcessStun();
            if (_isStunned == false)
            {
                move = ProcessMouseInput();
                move = ProcessKeyboardInput();
            }

            CheckColission();
            CheckMaxSpeed();
            CheckAreaBoundings();
            if (move == false)
                Decelerate();
            CatchNaNVector();

            _position += _direction;

        }


        private bool ProcessMouseInput()
        {
            _doShootMain = false;
            bool buttonPressed = false;
            if (InputManager.IsLeftMousePressed() && HudManager.HudButtonPressed == false)
            {
                _doShootMain = true;
            }
            if (InputManager.IsRightMousePressed())
            {
                Vector2 mousePos = InputManager.GetMouse().Position.ToVector2();

                float dist = MyMathHelper.CalcDistance(DrawingCenter(), mousePos);

                Vector2 inputDir = MyMathHelper.CalcDirection(DrawingCenter(), mousePos, dist);

                inputDir *= _speed * 2;
                _direction += inputDir;
                buttonPressed = true;
            }

            return buttonPressed;
        }
        private bool ProcessKeyboardInput()
        {
            KeyboardState keyState = InputManager.GetKeys();
            bool buttonPressed = false;

            if (keyState.IsKeyDown(Keys.D) || keyState.IsKeyDown(Keys.Right))
            {
                _direction.X += _speed;
                buttonPressed = true;
            }
            if (keyState.IsKeyDown(Keys.A) || keyState.IsKeyDown(Keys.Left))
            {
                _direction.X -= _speed;
                buttonPressed = true;
            }
            if (keyState.IsKeyDown(Keys.W) || keyState.IsKeyDown(Keys.Up))
            {
                _direction.Y -= _speed;
                buttonPressed = true;
            }
            if (keyState.IsKeyDown(Keys.S) || keyState.IsKeyDown(Keys.Down))
            {
                _direction.Y += _speed;
                buttonPressed = true;
            }
            if (keyState.IsKeyDown(Keys.E))
            {
                if (_timeSincePort >= _portRate)
                {
                    _doPort = true;
                }
            }
            if (keyState.IsKeyDown(Keys.Q))
            {
                if (_timeSinceAoe >= _aoeRate)
                {
                    _doAoe = true;
                }
            }

            return buttonPressed;
        }

        private bool CheckColission()
        {
            bool collided = false;
            int x1 = (int)DrawingCenter().X;
            int y1 = (int)DrawingCenter().Y;
            int r1 = _drawingSquare.Width / 3;
            foreach (Tower tower in _level._towers)
            {
                int x2 = (int)tower.DrawingCenter().X;
                int y2 = (int)tower.DrawingCenter().Y;
                int r2 = tower.drawingSquare().Width / 3;

                if (CollisionHelper.BoundingCircle(x1, y1, r1, x2, y2, r2))
                {
                    ProcessColission(tower);
                    collided = true;
                    break;
                }
            }
            return collided;
        }

        private void CheckAreaBoundings()
        {
            if (_position.X < 0)
            {
                _direction.X *= -1;
            }

            if (_position.Y < 0)
            {
                _direction.Y *= -1;
            }

            if (_position.X > 1600 - _drawingSquare.Width)
            {
                _direction.X *= -1;
            }

            if (_position.Y > 1000 - _drawingSquare.Height)
            {
                _direction.Y *= -1;
            }
        }
        private void CheckMaxSpeed()
        {
            if (_direction.X < _maxSpeed * -1)
            {
                _direction.X += _speed;
            }
            if (_direction.Y < _maxSpeed * -1)
            {
                _direction.Y += _speed;
            }

            if (_direction.X > _maxSpeed)
            {
                _direction.X -= _speed;
            }
            if (_direction.Y > _maxSpeed)
            {
                _direction.Y -= _speed;
            }
        }
        private void Decelerate()
        {
            _deceleration = Vector2.Negate(_direction) * _decelerationFactor;
            _direction += _deceleration;
        }
        private void CatchNaNVector()
        {
            if (_direction.X != _direction.X)
            {
                _position = _oldPosition + new Vector2(1.1f, 1.1f);
                _direction = new Vector2(-1, -1);
            }

        }

        private void ProcessColission(Tower tower)
        {
            float dist = MyMathHelper.CalcDistance(DrawingCenter(), tower.DrawingCenter());

            Vector2 tempDir = Vector2.Negate(MyMathHelper.CalcDirection(DrawingCenter(), tower.DrawingCenter(), dist));
            _direction = Vector2.Zero;
            _direction += tempDir;
        }
        private void ProcessColission(Creep creep)
        {
            float dist = MyMathHelper.CalcDistance(DrawingCenter(), creep.DrawingCenter());

            Vector2 tempDir = Vector2.Negate(MyMathHelper.CalcDirection(DrawingCenter(), creep.DrawingCenter(), dist));
            _direction = Vector2.Zero;
            _direction += tempDir;
        }

        public override void Update(GameTime gameTime)
        {
            CheckForEffects();
            getInput();

            base.Update(gameTime);
            if (_isStunned == false)
            {
                if (_doPort == true)
                {
                    Port();
                }
                else
                {
                    _timeSincePort++;
                }

                LinkedList<string> tempIds = _lastTargetIds;
                if (_defaultRldTimer.ElapsedMilliseconds >= _splashFireRate)
                {
                    Vector2 mousePos = new Vector2(InputManager.GetMouse().X, InputManager.GetMouse().Y);
                    if (_doShootMain == true)
                    {
                        BasicCreep temp = new BasicCreep(mousePos, FontSpriteManager.rawWhite, "temp", (int)act_Type.ENEMY, 1);
                        _userCannonTarget = temp;
                        ShootProjectile(temp, new PlayerSplashProjectile(DrawingCenter(), FontSpriteManager.circle, "splash", (int)act_Type.PROJECTILE, _level));
                        _defaultRldTimer.Restart();
                        _shotThisFrame = true;
                        _lastTarget = temp;
                    }
                }

                if (_autoRldTimer.ElapsedMilliseconds >= _autoFireRate)
                {
                    foreach (Creep target in _currentTargets)
                    {
                        if (tempIds.Contains(target.Id()) == false)
                        {
                            ShootProjectile(target, new SingleTargetProjectile(DrawingCenter(), FontSpriteManager.circle, "singletarget", (int)act_Type.PROJECTILE));
                            _lastTargetIds.AddFirst(target.Id());
                            _lastTarget = target;
                            _autoRldTimer.Restart();
                            break;
                        }
                        if (_lastTargetIds.Count >= _currentTargets.Count)
                        {
                            _lastTargetIds.Clear();
                        }
                    }
                    _shotThisFrame = true;
                }
                if (_doAoe == true)
                {
                    Color tempColor = Color.Red;
                    _aoeEmitter.ClearTargets();
                    _aoeEmitter.Update(DrawingCenter(), tempColor);
                    int i = 0;
                    LinkedList<Creep> tempTargets = new LinkedList<Creep>();
                    foreach (Creep creep in _level._creeps)
                    {
                        if (i >= _maxAoeTargets)
                        {
                            break;
                        }
                        if (CollisionHelper.BoundingCircle((int)DrawingCenter().X, (int)DrawingCenter().Y, RangeInPx, (int)creep.DrawingCenter().X, (int)creep.DrawingCenter().Y, 5))
                        {
                            if (creep._isStunned == false)
                            {
                                creep.GetStunned((int)_aoeDuration);
                                _aoeEmitter.AddTarget(creep.DrawingCenter(), Color.Red, 0.01f);
                                i++;
                            }
                        }
                    }
                    _doAoe = false;
                    _timeSinceAoe = 0;
                }
                else
                {
                    _timeSinceAoe++;
                }
                if (_timeSinceAoe > 1 && _timeSinceAoe < 120)
                {
                    if (_timeSinceAoe % 2 != 0)
                    {
                        _aoeEmitter.Update(_position, Color.Blue);
                    }
                    else
                    {
                        _aoeEmitter.Update(_position, Color.AliceBlue);
                    }
                }
                if (_timeSincePort > 1 && _timeSincePort < 120)
                {
                    _aoeEmitter.Update(_position, Color.Blue);
                    if (_timeSincePort < 60)
                    {
                        _portEmitter.Update(DrawingCenter(), new Vector2(0), Color.Blue);
                    }
                }
            }
        }

        private void Port()
        {
            Vector2 portPos = new Vector2(_drawingSquare.Width / 2, _drawingSquare.Height / 2);
            _aoeEmitter.ClearTargets();
            _aoeEmitter.Update(_position, _maskColor);
            _aoeEmitter.AddTarget(InputManager.GetMouse().Position.ToVector2());
            _aoeEmitter.AddTarget(InputManager.GetMouse().Position.ToVector2());
            _portEmitter.Update(DrawingCenter(), new Vector2(0), Color.Blue);

            //TODO... check for valid position
            _position = InputManager.GetMouse().Position.ToVector2() - portPos;
            _doPort = false;
            _timeSincePort = 0;
        }

        private void CheckForEffects()
        {
            ProcessStun();
        }

        private void ProcessStun()
        {
            if (_isStunned == true)
            {
                _timeSinceStun++;
                _maskColor = Color.Tomato;
                if (_timeSinceStun >= _maxStunTime)
                {
                    _isStunned = false;
                    _timeSinceStun = 0;
                    _timeSinceLastStun = 0;
                    _maskColor = _defaultColor;
                }
            }
            else
            {
                _timeSinceLastStun++;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (_timeSincePort <= 30)
            {
                _portEmitter.Draw(spriteBatch);
                _aoeEmitter.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);
            if (_timeSinceAoe > 1 && _timeSinceAoe < 120)
            {
                _aoeEmitter.Draw(spriteBatch);
            }
        }
        protected override void DrawProgressBars(SpriteBatch spriteBatch)
        {
            base.DrawProgressBars(spriteBatch);

            float portProgress;
            if (_timeSincePort <= _portRate)
            {
                portProgress = (_timeSincePort / _portRate);
            }
            else
            {
                portProgress = 1;
            }
            if (_timeSincePort <= _portRate)
            {
                Vector2 progessBar = new Vector2(portProgress, 20);
                Vector2 offset = new Vector2(0, _drawingSquare.Height - 10);
                DrawHelper.DrawProgressBar(spriteBatch, _drawingSquare.Location.ToVector2() + offset, new Vector2(_drawingSquare.Width, 10), portProgress, 1, Color.Yellow, Color.Black);
            }

            float aoeProgress;
            if (_timeSinceAoe <= _aoeRate)
            {
                aoeProgress = (_timeSinceAoe / _aoeRate);
            }
            else
            {
                aoeProgress = 1;
            }
            if (_timeSinceAoe <= _aoeRate)
            {
                Vector2 progessBar = new Vector2(aoeProgress, 20);
                Vector2 offset = new Vector2(0, 0);
                DrawHelper.DrawProgressBar(spriteBatch, _drawingSquare.Location.ToVector2() + offset, new Vector2(_drawingSquare.Width, 10), aoeProgress, 1, Color.Gold, Color.Black);
            }
        }

        protected override void DrawTargetMarkers(SpriteBatch spriteBatch)
        {
            foreach (Creep target in _currentTargets)
            {
                DrawHelper.DrawCuttedLine(spriteBatch, target.DrawingCenter(), DrawingCenter(), 8, 8, Color.Yellow);
            }
        }


        public override float Firerate()
        {
            return (float)Math.Round((double)_autoFireRate / 1000, 2);
        }
    }
}
