﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    public class SplashTower : Tower
    {
        public SplashTower(Vector2 position, Texture2D sprite, string id, int actorType, TestLevel level) : base(position, sprite, id, actorType, level)
        {
            _timeForUpgrade = 1500;
            //_maxTwrLvl = 8;
            _autoFireRate = 2000;
            RangeInPx = 150;
            _drawingSquare.Size = new Point(50);
            _maskColor= new Color(173, 255, 164);
            _maxTargets = 1;
            _splashDmgPerShot = 35;
            _critChance = 0;
            _critMultiplier = (float)(Math.E / 2);
            UpgradeCost = 18;
            Type = "splash";
        }

        public override void Initialize(bool updateEvenFrame)
        {
            base.Initialize(updateEvenFrame);

        }

        public override void Upgrade()
        {
            UpgradeCost *= (float)Math.E;

            _timeForUpgrade *= 2;

            _splashDmgPerShot *= 1.45f;
            _splashDmgPerShot += 7;

            _critMultiplier *= 1.4f;

            if (_autoFireRate - 150 > 500)
            {
                _autoFireRate -= 150;
            }

            if (_maxTargets < 3)
            {
                _maxTargets += 1;
            }

            RangeInPx += 12;
            switch (_towerLvl)
            {
                case 1:
                    _maskColor = new Color(173, 255, 164);

                    break;

                case 2:
                    _maskColor = new Color(126, 255, 112);
                    break;

                case 3:

                    _maskColor = new Color(24, 255, 0);
                    break;

                case 4:
                    _maskColor = new Color(17, 178, 0);
                    break;

                case 5:
                    _maskColor = new Color(13, 137, 0);
                    _autoFireRate -= 120;

                    break;

                case 6:
                    _maskColor = new Color(13, 137, 0);
                    break;
            }
           
            base.Upgrade();
        }



        public override void getInput()
        {

            base.getInput();

        }

        public override void Update(GameTime gameTime)
        {
            getInput();
            if (_isActive == true)
            {
                base.Update(gameTime);
                LinkedList<string> tempIds = _lastTargetIds;
                if (_defaultRldTimer.ElapsedMilliseconds >= _autoFireRate)
                {
                    foreach (Creep target in _currentTargets)
                    {
                        if (tempIds.Contains(target.Id()) == false)
                        {
                            ShootProjectile(target, new SplashProjectile(DrawingCenter(), FontSpriteManager.circle, "splash", (int)act_Type.PROJECTILE, _level));
                            _lastTargetIds.AddFirst(target.Id());
                            _lastTarget = target;
                            _defaultRldTimer.Restart();
                            break;
                        }
                        if (_lastTargetIds.Count >= _currentTargets.Count)
                        {
                            _lastTargetIds.Clear();
                        }
                    }
                    _shotThisFrame = true;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            base.Draw(spriteBatch);

        }
    }
}
