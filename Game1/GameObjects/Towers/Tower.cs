﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace Game1
{
    public abstract class Tower : GameActor
    {

        protected TestLevel _level;

        public string Type;

        public int RangeInPx;
        protected int _maxTargets;
        protected int _countTargets;
        protected int _towerLvl;
        protected int _maxTwrLvl;
        public int EffectDuration;


        protected float _timeSinceLastStun;
        protected float _timeSinceStun;
        protected float _maxStunTime;

        protected float _autoFireRate;
        protected float _splashFireRate;

        protected float _splashDmgPerShot;
        protected float _singleTargetDmgPerShot;

        protected float _critMultiplier;
        protected float _critChance;

        protected float _timeForUpgrade;
        public float UpgradeCost;

        protected bool _isStunned;
        protected bool _shotThisFrame;

        protected Stopwatch _defaultRldTimer;
        protected Stopwatch _upgradeTimer;
        protected Stopwatch _autoRldTimer;


        protected LinkedList<string> _lastTargetIds;
        public LinkedList<Creep> _currentTargets;
        protected LinkedList<Projectile> _activeProjectiles;
        protected Creep _lastTarget;
        protected Vector2 _lastAim;

        public Tower(Vector2 position, Texture2D sprite, string id, int actorType, TestLevel level) : base(position, sprite, id, actorType)
        {
            _level = level;
            _towerLvl = 1;
            _maxTwrLvl = 6;
        }

        public virtual void SetNextLevelStage(TestLevel level)
        {
            _level = level;
            HudManager.RemoveButton(Id());
            HudManager.AddButton(Id(), new Button(this, new Vector2(_drawingSquare.Width - 6, 20), "extrasmall hold", "+" + ((int)UpgradeCost).ToString(), 30, _maskColor));

            _lastTargetIds = new LinkedList<string>();
            _currentTargets = new LinkedList<Creep>();
            _countTargets = 0;
            _activeProjectiles = new LinkedList<Projectile>();
            _isActive = true;
            _defaultRldTimer = new Stopwatch();
            _upgradeTimer = new Stopwatch();
            _upgradeTimer.Start();
            _lastTarget = null;
            _defaultRldTimer.Start();
            _shotThisFrame = false;
            _lastAim = new Vector2(0, -1);
            _isStunned = false;
            _direction = new Vector2(0, 0);
            _autoRldTimer = new Stopwatch();
            _autoRldTimer.Start();
        }
        public virtual void Initialize(bool updateEvenFrame)
        {
            HudManager.AddBackgroundButton(Id(), new Button(this, new Vector2(_drawingSquare.Width - 6, 20), "extrasmall hold", "+" + UpgradeCost.ToString(), 30, _maskColor));
            if (_maxTwrLvl > _level.Id + 4)
            {
                _maxTwrLvl = _level.Id + 4;
            }
            _updateEvenFrame = updateEvenFrame;
            _lastTargetIds = new LinkedList<string>();
            _currentTargets = new LinkedList<Creep>();
            _activeProjectiles = new LinkedList<Projectile>();
            _isActive = true;
            _defaultRldTimer = new Stopwatch();
            _upgradeTimer = new Stopwatch();
            _upgradeTimer.Start();
            _lastTarget = null;
            _defaultRldTimer.Start();
            _shotThisFrame = false;
            _lastAim = new Vector2(0, -1);
            _towerLvl = 1;

            _isStunned = false;
        }

        public virtual void getInput()
        {
            CheckForUpgradeClick();
        }

        protected void CheckForUpgradeClick()
        {
            if (_upgradeTimer.ElapsedMilliseconds >= _timeForUpgrade)
            {
                if (IsUpgradable())
                {
                    if (HudManager.GetButtonPress(Id()))
                    {
                        if (_level.BuyTower(this) == true)
                        {
                            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Tower Upgrade: $" + (int)UpgradeCost, Color.LightBlue);
                            Upgrade();
                        }
                    }
                }
                else if (HudManager.ContainsButton(Id()))
                {
                    HudManager.RemoveBackgroundButton(Id());
                }
            }
        }

        protected virtual bool IsUpgradable()
        {
            if (_towerLvl < _maxTwrLvl)
                return true;
            else
                return false;
        }

        public virtual void Upgrade()
        {
            _towerLvl += 1;
            _upgradeTimer.Restart();
            _defaultColor = _maskColor;
            HudManager.RemoveButton(Id());
            if (_towerLvl < _maxTwrLvl)
            {
                HudManager.AddButton(Id(), new Button(this, new Vector2(_drawingSquare.Width - 6, 20), "extrasmall hold", "+" + ((int)(UpgradeCost)).ToString(), 30, Color.WhiteSmoke));
            }
            if (_splashFireRate > 0)
            {
                DebugHelper.AddLog(MyMathHelper.GetGuid(), "Firerate: " + (int)(_splashFireRate / 10), Color.GhostWhite);
                DebugHelper.AddLog(MyMathHelper.GetGuid(), "Tower Upgrade: $" + UpgradeCost, Color.GhostWhite);
            }
            if (_autoFireRate > 0)
            {
                DebugHelper.AddLog(MyMathHelper.GetGuid(), "Firerate: " + (int)(_autoFireRate / 10), Color.GhostWhite);
                DebugHelper.AddLog(MyMathHelper.GetGuid(), "Damage: " + (int)_singleTargetDmgPerShot, Color.GhostWhite);
            }
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "MaxTargets: " + _maxTargets, Color.GhostWhite);
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "Range: " + RangeInPx, Color.GhostWhite);



        }


        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (_lastTarget != null)
            {
                _lastAim = _lastTarget.DrawingCenter();
            }
            if (_currentTargets.Count == 0)
            {
                _lastTarget = null;
            }
            _shotThisFrame = false;
            LinkedList<Projectile> toRemove = new LinkedList<Projectile>();
            foreach (Projectile projectile in _activeProjectiles)
            {
                if (projectile.IsDead() == true)
                {
                    toRemove.AddFirst(projectile);
                }
                else
                {
                    projectile.Update(gameTime);
                }
            }

            foreach (Projectile p in toRemove)
            {
                _activeProjectiles.Remove(p);
            }
        }


        public override void Draw(SpriteBatch spriteBatch)
        {
            //DrawRectangleTowerLook(spriteBatch);
            base.Draw(spriteBatch);

            if (_isActive == true)
            {
                DrawAim(spriteBatch);
                DrawProjectiles(spriteBatch);
                DrawTargetMarkers(spriteBatch);
                DrawProgressBars(spriteBatch);
                DrawTextInfo(spriteBatch);
            }
        }

        protected virtual void DrawProjectiles(SpriteBatch spriteBatch)
        {
            foreach (Projectile projectile in _activeProjectiles)
            {
                projectile.Draw(spriteBatch);
            }
        }

        protected virtual void DrawAim(SpriteBatch spriteBatch)
        {
            foreach (Creep target in _currentTargets)
            {
                DrawHelper.DrawCuttedLine(spriteBatch, DrawingCenter(), target.DrawingCenter(), 4, 43, Color.Black);
            }
            DrawHelper.DrawCuttedLine(spriteBatch, DrawingCenter(), _lastAim, 8, 35, Color.Red);
        }

        protected virtual void DrawTextInfo(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(FontSpriteManager.fontSmall, _towerLvl.ToString(), _position + _drawingSquare.Size.ToVector2() + new Vector2(-13, -23), Color.Black);
        }

        protected virtual void DrawTargetMarkers(SpriteBatch spriteBatch)
        {
            foreach (Creep target in _currentTargets)
            {
                DrawHelper.DrawCuttedLine(spriteBatch, target.DrawingCenter(), DrawingCenter(), 4, 4, Color.Red);
            }
        }

        protected virtual void DrawRectangleTowerLook(SpriteBatch spriteBatch)
        {
            DrawHelper.DrawFilledRectangle(spriteBatch, _drawingSquare.Location.ToVector2(), _drawingSquare.Size.ToVector2(), _angle, _maskColor);
            DrawHelper.DrawEmptyRectangle(spriteBatch, _drawingSquare.Location.ToVector2(), _drawingSquare.Size.ToVector2(), 3, Color.Black);
        }

        protected virtual void DrawProgressBars(SpriteBatch spriteBatch)
        {
            float reloadProgress;
            if (_defaultRldTimer.ElapsedMilliseconds <= _splashFireRate)
            {
                reloadProgress = (_defaultRldTimer.ElapsedMilliseconds / _splashFireRate);
            }
            else
            {
                reloadProgress = 1;
            }
            if (_defaultRldTimer.ElapsedMilliseconds <= _splashFireRate)
            {
                Vector2 progessBar = new Vector2(10, reloadProgress);
                Vector2 offset = new Vector2(-_drawingSquare.Width / 2, _drawingSquare.Height / 2);
                DrawHelper.DrawProgressBar(spriteBatch, DrawingCenter() + offset, new Vector2(_drawingSquare.Width, 10), reloadProgress, 1);
            }

            float upgradeProgress;
            if (_upgradeTimer.ElapsedMilliseconds <= _timeForUpgrade)
            {
                upgradeProgress = (_upgradeTimer.ElapsedMilliseconds / _timeForUpgrade);
            }
            else
            {
                upgradeProgress = 1;
            }
            if (_upgradeTimer.ElapsedMilliseconds <= _timeForUpgrade)
            {
                Vector2 progessBar = new Vector2(upgradeProgress, 20);
                Vector2 offset = new Vector2(-_drawingSquare.Width / 2, -_drawingSquare.Height / 2);
                DrawHelper.DrawProgressBar(spriteBatch, DrawingCenter() + offset, new Vector2(_drawingSquare.Width, 10), upgradeProgress, 1);
            }
        }

        public bool ShootProjectile(Creep enemy, Projectile projectile)
        {
            bool isCrit = false;

            float critDamage = 0;
            //projectile.SetColor(_maskColor);

            if (MyMathHelper.GetRandom(0, 100) <= _critChance)
            {
                isCrit = true;
            }

            switch (projectile.Id())
            {
                case "aoefreeze":
                    //nooone
                    break;

                case "singlefreeze":
                    if (isCrit == true)
                    {
                        critDamage = _singleTargetDmgPerShot * _critMultiplier;
                        projectile.SetDamage((int)critDamage);
                        projectile.EffectDuration = EffectDuration;
                    }
                    else
                    {
                        projectile.SetDamage((int)_singleTargetDmgPerShot);
                        projectile.EffectDuration = EffectDuration;
                    }
                    break;

                case "splash":
                    if (isCrit == true)
                    {
                        critDamage = _splashDmgPerShot * _critMultiplier;
                        projectile.SetDamage((int)critDamage);
                    }
                    else
                    {
                        projectile.SetDamage((int)_splashDmgPerShot);
                    }
                    break;

                case "singletarget":
                    if (isCrit == true)
                    {
                        critDamage = _singleTargetDmgPerShot * _critMultiplier;
                        projectile.SetDamage((int)critDamage);
                    }
                    else
                    {
                        projectile.SetDamage((int)_singleTargetDmgPerShot);
                    }
                    break;
            }

            projectile.Intitialize(enemy);
            _activeProjectiles.AddFirst(projectile);
            return true;
        }

        public virtual bool AddTarget(Creep target)
        {
            if (_currentTargets.Contains(target) == false)
            {
                _currentTargets.AddFirst(target);
                _countTargets++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool RemoveTarget(Creep target)
        {
            if (_currentTargets.Contains(target))
            {
                _currentTargets.Remove(target);
                _countTargets--;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool HasMaxTargets()
        {
            if (_countTargets >= _maxTargets)
                return true;
            else
                return false;
            //if (_currentTargets.Count >= _maxTargets)
            //    return true;
            //else
            //    return false;
        }

        public int ActiveProjectilesCount()
        {
            return _activeProjectiles.Count;
        }

        public virtual float Firerate()
        {
            return (float)Math.Round((double)_splashFireRate / 1000, 2);
        }

        public void StunMe()
        {
            if (_isStunned == false)
            {
                if (_timeSinceLastStun >= _maxStunTime)
                {
                    _isStunned = true;
                }
            }
        }
    }
}
