﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Game1
{
    public class AnimatedSprite
    {
        public Texture2D Texture { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }
        private int currentFrame;
        private int totalFrames;

        private int currentDirection;

        public AnimatedSprite(Texture2D texture, int rows, int columns)
        {
            Texture = texture;
            Rows = rows;
            Columns = columns;
            currentFrame = 0;
            totalFrames = Rows * Columns;
        }

        public void Update()
        {
            currentFrame++;
            if (currentFrame == totalFrames)
                currentFrame = 0;
        }

        public void Update(int direction)
        {
            if (direction == 0)
            {
                currentFrame = 0;
            }
            else if (currentDirection == direction)
            {
                totalFrames = Columns;
                currentFrame++;
                if (currentFrame == totalFrames)
                    currentFrame = 0;
            }
            else
            {
                currentFrame = 0;
                currentDirection = direction;
            }

        }

        public void Draw(SpriteBatch spriteBatch, Rectangle destinationRectangle,float scale, Color maskColor)
        {
            int width = Texture.Width / Columns;
            int height = Texture.Height / Rows;
            int row = (int)((float)currentFrame / (float)Columns);
            int column = currentFrame % Columns;

            Rectangle sourceRectangle = new Rectangle(width * column, height * row, width, height);
            spriteBatch.Draw(Texture, destinationRectangle, sourceRectangle, maskColor);
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 location, int direction, double size, Color maskColor)
        {
            int width = Texture.Width / Columns;
            int height = Texture.Height / Rows;
            int row = direction;
            int column = currentFrame % Columns;

            Rectangle sourceRectangle = new Rectangle(width * column, height * row, width, height);
            double sizedWidth = width * size;
            width = (int)sizedWidth;
            double sizedHeight = height * size;
            height = (int)sizedHeight;
            Rectangle destinationRectangle = new Rectangle((int)location.X, (int)location.Y, width, height);
            spriteBatch.Draw(Texture, destinationRectangle, sourceRectangle, maskColor);
        }
    }
}
