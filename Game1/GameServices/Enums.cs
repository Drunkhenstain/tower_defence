﻿namespace Game1
{
    enum act_userInput
    {
        RIGHT,
        LEFT,
        UP,
        DOWN,
        MOUSEFLY,
        ATTACK,
        INSPECT,
        NONE
    }
    enum act_Type
    {
        ENEMY,
        TOWER,
        PROJECTILE
    }
    enum act_crashedInto
    {
        ENEMY,
        NONE
    }
    enum act_AnimType
    {
        NONE,
        STATIC,
        DYNAMIC
    }
    enum act_Direction
    {
        NONE,
        UP,
        RIGHT,
        DOWN,
        LEFT
    }
    enum act_PlayerState
    {

    }
}
