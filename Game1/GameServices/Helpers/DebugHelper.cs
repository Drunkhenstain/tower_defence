﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Game1
{
    public static class DebugHelper
    {
        private static Dictionary<string, string> _logHistory = new Dictionary<string, string>();
        private static Dictionary<string, Color> _logColors = new Dictionary<string, Color>();

        private static int _maxLogsDisplayed = 14;
        private static int _displayIndex = 0;
        private static Vector2 _position = new Vector2(1605, 260);
        private static Vector2 _size = new Vector2(195, 250);

        private static bool _mouseOver = false;

        public static void Start()
        {
            AddLog("start1", "WELCOME TO THE ONLY", Color.White);
            AddLog("start2", "   GAME YOU WILL   ", Color.White);
            AddLog("start3", " EVER NEED TO PLAY ", Color.White);
            AddLog("start4", "    TO BE HAPPY    ", Color.White);
        }

        public static void Update()
        {
            CheckForScroll();
        }

        private static void CheckForScroll()
        {
            _mouseOver = false;
            if (CollisionHelper.BoundingRectangle(new Rectangle(_position.ToPoint(), _size.ToPoint()), new Rectangle(InputManager.GetMouse().Position, new Point(1))))
            {
                _mouseOver = true;
                _displayIndex += InputManager.GetScrollValue();

                if (_displayIndex < 0)
                {
                    _displayIndex = 0;
                }
                if (_displayIndex > _logHistory.Count - 3)
                {
                    _displayIndex = _logHistory.Count - 3;
                }

                if (InputManager.IsLeftMousePressed())
                {
                    SetBottomIndex();
                }
            }
        }

        private static void SetBottomIndex()
        {
            if (_logHistory.Count > _maxLogsDisplayed)
            {
                _displayIndex = _logHistory.Count - _maxLogsDisplayed;
            }
        }

        public static void AddLog(string key, string text, Color txtColor)
        {
            string finalMsg = "";
            finalMsg += text;

            _logHistory.Add(key, finalMsg);
            _logColors.Add(key, txtColor);

            if (_mouseOver == false)
            {
                SetBottomIndex();
            }
        }

        public static void ClearCompleteLogHistory()
        {
            _logColors.Clear();
            _logHistory.Clear();
        }
        public static void RemoveLog(string key)
        {
            _logColors.Remove(key);
            _logHistory.Remove(key);
        }

        public static void DrawLogWithColor(SpriteBatch sB)
        {
            DrawHelper.DrawFilledRectangle(sB, _position + new Vector2(-3, -3), _size, 0, Color.Black);
            DrawHelper.DrawEmptyRectangle(sB, _position + new Vector2(-5, -5), _size+new Vector2(3), 2, Color.White);

            int startLog = _displayIndex;
            int logCount = 0;

            Vector2 colOffset = new Vector2(0, 0);
            foreach (KeyValuePair<string, string> logInfo in _logHistory)
            {
                if (logCount >= startLog + _maxLogsDisplayed)
                    break;

                if (logCount >= startLog)
                {
                    DrawHelper.DrawExtraSmallText(sB, logInfo.Value, _position + colOffset, 1, 0, _logColors[logInfo.Key]);
                    colOffset.Y += 16;
                    logCount++;
                }
                else
                {
                    logCount++;
                }
            }
        }
    }
}
