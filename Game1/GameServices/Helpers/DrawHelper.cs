﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    public static class DrawHelper
    {
        public static void DrawProgressBar(SpriteBatch sb, Vector2 finalPosition, Vector2 size, float progress, float scale)
        {
            Vector2 vecProgress = new Vector2(size.X * progress, size.Y);
            DrawHelper.DrawFilledRectangle(sb, finalPosition, size * scale, 0, Color.Red);
            DrawHelper.DrawFilledRectangle(sb, finalPosition, vecProgress * scale, 0, Color.Green);
            DrawHelper.DrawEmptyRectangle(sb, finalPosition, size * scale, 1, Color.Black);
        }
        public static void DrawProgressBar(SpriteBatch sb, Vector2 finalPosition, Vector2 size, float progress, float scale,Color fgColor,Color bgColor)
        {
            Vector2 vecProgress = new Vector2(size.X * progress, size.Y);
            DrawHelper.DrawFilledRectangle(sb, finalPosition, size * scale, 0, bgColor);
            DrawHelper.DrawFilledRectangle(sb, finalPosition, vecProgress * scale, 0, fgColor);
            DrawHelper.DrawEmptyRectangle(sb, finalPosition, size * scale, 1, Color.Black);
        }

        public static void DrawExtraSmallTextWithLabel(SpriteBatch sb, Vector2 finalPosition, Vector2 size, string text, float scale, Color color)
        {
            DrawHelper.DrawFilledRectangle(sb, finalPosition, size, 0, color);
            DrawHelper.DrawEmptyRectangle(sb, finalPosition, size, 3, Color.Black);
            DrawHelper.DrawExtraSmallText(sb, text, finalPosition+new Vector2(2) , 1, 0, Color.Black);
        }
        public static void DrawSmallTextWithLabel(SpriteBatch sb, Vector2 finalPosition, Vector2 size, string text, float scale, Color color)
        {
            Vector2 offset = new Vector2(5, 3);

            DrawHelper.DrawFilledRectangle(sb, finalPosition, size, 0, color);
            DrawHelper.DrawEmptyRectangle(sb, finalPosition, size, 3, Color.Black);
            DrawHelper.DrawSmallText(sb, text, finalPosition + offset, 1, 0, Color.Black);
        }
        public static void DrawTextWithLabel(SpriteBatch sb, Vector2 finalPosition, Vector2 size, string text, float scale,Color color)
        {
            Vector2 offset = new Vector2(0, 0);

            DrawHelper.DrawFilledRectangle(sb, finalPosition, size, 0, color);
            DrawHelper.DrawEmptyRectangle(sb, finalPosition, size, 3, Color.Black);
            DrawHelper.DrawText(sb,text, finalPosition +offset, 1, 0, Color.Black);
        }
                
        public static void DrawFilledRectangle(SpriteBatch sb, Vector2 position, Vector2 size, float angle, Color color)
        {
            sb.Draw(FontSpriteManager.rawWhite, new Rectangle(position.ToPoint(), size.ToPoint()), null, color, angle, new Vector2(0, 0), SpriteEffects.None, 0);
        }
        public static void DrawEmptyRectangle(SpriteBatch sb, Vector2 position, Vector2 size, int strokeWidthInPx, Color color)
        {
            Vector2 topLeft = position;
            Vector2 topRight = new Vector2(topLeft.X + size.X, topLeft.Y);
            Vector2 bottomLeft = new Vector2(topLeft.X, topLeft.Y + size.Y);
            Vector2 bottomRight = new Vector2(topLeft.X + size.X, bottomLeft.Y);

            DrawFullLine(sb, topLeft, topRight, strokeWidthInPx, color);

            DrawFullLine(sb, topRight, bottomRight, strokeWidthInPx, color);

            DrawFullLine(sb, bottomRight, bottomLeft, strokeWidthInPx, color);

            DrawFullLine(sb, bottomLeft, topLeft, strokeWidthInPx, color);
        }

        public static void DrawCuttedLine(SpriteBatch sb, Vector2 start, Vector2 end, int strokeWidthInPx, int strokeLengthInPx, Color color)
        {
            Vector2 edge = end - start;
            // calculate angle to rotate line
            float angle = (float)Math.Atan2(edge.Y, edge.X);


            sb.Draw(FontSpriteManager.rawWhite, new Rectangle((int)start.X, (int)start.Y, strokeLengthInPx, strokeWidthInPx), null, color, angle, new Vector2(0, 0), SpriteEffects.None, 0);
        }
        public static void DrawFullLine(SpriteBatch sb, Vector2 start, Vector2 end, int strokeWidthInPx, Color color)
        {
            Vector2 edge = end - start;
            // calculate angle to rotate line
            float angle = (float)Math.Atan2(edge.Y, edge.X);
            sb.Draw(FontSpriteManager.rawWhite, new Rectangle((int)start.X, (int)start.Y, (int)edge.Length(), strokeWidthInPx), null, color, angle, new Vector2(0, 0), SpriteEffects.None, 0);

        }

        public static void DrawExtraSmallText(SpriteBatch sb, string text, Vector2 position, float scale, float angle, Color color)
        {
            sb.DrawString(FontSpriteManager.fontExtraSmall, text, position, color/*, angle, position, scale,, SpriteEffects.None*/);
        }
        public static void DrawSmallText(SpriteBatch sb, string text, Vector2 position, float scale, float angle, Color color)
        {
            sb.DrawString(FontSpriteManager.fontSmall, text, position, color/*, angle, position, scale,, SpriteEffects.None*/);
        }
        public static void DrawText(SpriteBatch sb, string text, Vector2 position, float scale, float angle, Color color)
        {
            sb.DrawString(FontSpriteManager.fontMid, text, position, color/*, angle, position, scale,, SpriteEffects.None*/);
        }
        
        public static Color GetRandomColor()
        {
            return new Color(
                (byte)MyMathHelper.GetRandom(20, 235),
                (byte)MyMathHelper.GetRandom(20, 235),
                (byte)MyMathHelper.GetRandom(20, 235));
        }
    }
}
