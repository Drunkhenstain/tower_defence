﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    public static class MyMathHelper
    {
        static Random random = new Random();

        public static float CalcDistance(Vector2 start, Vector2 dest)
        {
            return (float)Math.Sqrt(Math.Pow(dest.X - start.X, 2) + Math.Pow(dest.Y - start.Y, 2));
        }

        public static Vector2 CalcDirection(Vector2 start, Vector2 dest, float distance)
        {
            float directionX = (dest.X - start.X) / distance;
            float directionY = (dest.Y - start.Y) / distance;
            return new Vector2(directionX, directionY);
        }

        public static Vector2 CalcSmoothMovement(Vector2 position, Vector2 direction, float speedInPx, float timeSinceLastFrameInSec)
        {
            float destX = position.X + (direction.X * speedInPx * timeSinceLastFrameInSec);
            float destY = position.Y + (direction.Y * speedInPx * timeSinceLastFrameInSec);

            return new Vector2(destX, destY);
        }

        public static float GetAngle(Vector2 start, Vector2 end)
        {
            Vector2 edge = end - start;
            // calculate angle to rotate line
            float angle = (float)Math.Atan2(edge.Y, edge.X);
            return angle;
        }

        public static int GetRandom(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        public static Random Rand()
        {
            return random;
        }

        public static string GetGuid()
        {
            Guid guid = Guid.NewGuid();
            return guid.ToString();
        }

        //public static Vector2 calcTrackHeading(Vector2 myPos, Vector2 myVel, Vector2 target, Vector2 targetVel, float acceleration)
        //{
        //    float a = acceleration;

        //    Vector2 toFollow = target - myPos;
        //    float d = MyMathHelper.CalcDistance(myPos, target);

        //    // his speed relative to me
        //    Vector2 vTarget = targetVel - myVel;

        //    // Find the component of -vTarget along toFollow
        //    float v = Vector2.Dot(-vTarget, toFollow);

        //    // TIME ESTIMATE TO REACH MY TARGET
        //    float eta = -v / a + (float)Math.Sqrt(v * v / (a * a) + 2 * d / a);
        //    // Use ETA to estimate WHERE we should aim.

        //    Vector2 impactPos = target + vTarget * eta;

        //    return impactPos - myPos; // ME TO IMPACTPT is suggested heading.
        //}
    }
}
