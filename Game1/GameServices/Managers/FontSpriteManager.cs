﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    public static class FontSpriteManager
    {
        public static Texture2D rawWhite;

        public static Texture2D testBackground;
        public static Texture2D testBackground2;

        public static Texture2D largeBackground;
        public static Texture2D largeBackground2;
        public static Texture2D largeBackground3;
        public static Texture2D largeBackground4;
        public static Texture2D largeBackground5;
        public static Texture2D largeBackground6;
        public static Texture2D largeBackground7;
        public static Texture2D largeBackground8;
        public static Texture2D largeBackground9;

        public static Texture2D explosion5x5;

        public static Texture2D circle;
        public static Texture2D triangle;

        public static Texture2D asteroid;
        public static Texture2D earth;

        public static Texture2D userShip;

        public static Texture2D creep1; 
        public static Texture2D uglybaxa;

        public static Texture2D ship1;
        public static Texture2D ship2;
        public static Texture2D ship3;
        public static Texture2D ship4;
        public static Texture2D ship5;

        public static Texture2D HalfCircle;
        public static Texture2D LightningSegment;

        public static Texture2D GetRandomShip()
        {
            int rnd = MyMathHelper.GetRandom(1, 5);

            switch (rnd)
            {

                case 1:
                    return ship1;

                case 2:
                    return ship2;

                case 3:
                    return ship3;

                case 4:
                    return ship4;

                case 5:
                    return ship5;

                default:

                    return ship1;
            }
        }


        public static Texture2D bullet;
        public static Texture2D bullet2;
        public static Texture2D bullet3;


        public static Texture2D redCircle;
        public static Texture2D blueCircle;
        public static Texture2D greenCircle;

        public static SpriteFont fontMid;
        public static SpriteFont fontSmall;
        public static SpriteFont fontExtraSmall;

    }
}
