﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace Game1
{
    public static class GameManager
    {
        private static Stopwatch _changeLvlTimer = new Stopwatch();
        private static Stopwatch _beginLvlTimer = new Stopwatch();
        private static Stopwatch _resetLvlTimer = new Stopwatch();

        public static Tower UserTower;

        private static Dictionary<int, TestLevel> _levels = new Dictionary<int, TestLevel>();

        private static TestLevel _currentLevel;

        private static StackFiniteStateMachine _gameBrain;

        private static List<string> _stateHistory = new List<string>();

        private static LightningEmitter _lightEmitter;

        private static Vector2 _mainBtnOffset = new Vector2();

        public static int _currentLvlIndex;

        public static int _lvlsPlayedInRow;
        private static int _minWavesToPlay = 10;

        public static int _coinsLastLvl;
        public static int PlayerCoins;
        public static int _startCountdown = 1600;

        public static int _resetLvlCountdown = 1000;
        public static int _changeLvlCountdown = 400;

        private static string _currentState;
        private static string _lastState;

        private static bool _runGame = true;
        public static bool ExitGame = false;

        public static void InitializeGame()
        {
            DebugHelper.Start();
            /// Read Level Data From Svg Files And
            /// Create Levels
            HudManager.ResetHud();
            _lightEmitter = new LightningEmitter(new Vector2(50, 100), 0.1f);

            CreateMainMenuButtons();
            CreateUiButtons();
            CreateLevels();

            _currentLvlIndex = 1;

            _levels[_currentLvlIndex].Reset();

            _currentLevel = _levels[_currentLvlIndex];

            UserTower = new PlayerTower(new Vector2(50, 50), FontSpriteManager.userShip, "userTower", (int)act_Type.TOWER, _currentLevel);

            _changeLvlTimer.Start();
            _resetLvlTimer.Start();

            _gameBrain = new StackFiniteStateMachine();
            GotoMainMenu();
        }

        private static void CreateMainMenuButtons()
        {
            HudManager.AddMainMenuButton("1", new Button(new Vector2(50, 100), new Vector2(500, 270), "sprite", "Level: 1", FontSpriteManager.largeBackground, Color.White));
            HudManager.AddMainMenuButton("2", new Button(new Vector2(570, 100), new Vector2(500, 270), "sprite", "Level: 2", FontSpriteManager.largeBackground7, Color.White));
            HudManager.AddMainMenuButton("3", new Button(new Vector2(1090, 100), new Vector2(500, 270), "sprite", "Level: 3", FontSpriteManager.largeBackground2, Color.White));

            HudManager.AddMainMenuButton("4", new Button(new Vector2(50, 390), new Vector2(500, 270), "sprite", "Level: 4", FontSpriteManager.largeBackground5, Color.White));
            HudManager.AddMainMenuButton("5", new Button(new Vector2(570, 390), new Vector2(500, 270), "sprite", "Level: 5", FontSpriteManager.largeBackground3, Color.White));
            HudManager.AddMainMenuButton("6", new Button(new Vector2(1090, 390), new Vector2(500, 270), "sprite", "Level: 6", FontSpriteManager.largeBackground4, Color.White));

            HudManager.AddMainMenuButton("7", new Button(new Vector2(50, 680), new Vector2(500, 270), "sprite", "Level: 7", FontSpriteManager.largeBackground6, Color.White));
        }

        private static void CreateUiButtons()
        {
            HudManager.AddButton("pause", new Button(new Vector2(1632, 550), new Vector2(135, 45), "normal", "Pause", new Color(27, 96, 178)));
            HudManager.AddButton("wave", new Button(new Vector2(1632, 600), new Vector2(135, 45), "hold", "Spawn", 60, new Color(27, 96, 178)));
            HudManager.AddButton("change", new Button(new Vector2(1632, 700), new Vector2(135, 45), "hold", "Next", new Color(27, 96, 178)));
            HudManager.AddButton("restart", new Button(new Vector2(1632, 750), new Vector2(135, 45), "hold", "Restart", new Color(27, 96, 178)));
            HudManager.AddButton("mainmenu", new Button(new Vector2(1632, 850), new Vector2(135, 45), "hold", "Main", new Color(27, 96, 178)));
            HudManager.AddButton("exit", new Button(new Vector2(1632, 900), new Vector2(135, 45), "hold", "Exit", new Color(27, 96, 178)));
        }

        private static void CreateLevels()
        {
            MySvgReadAndParser.ReadAndAddFile("D:/micha/Documents/MonoGameProjekte/TowerDefenceTEST/Game1/Content/LogicMap/testMapLarge.svg", "test1");
            MySvgReadAndParser.ReadAndAddFile("D:/micha/Documents/MonoGameProjekte/TowerDefenceTEST/Game1/Content/LogicMap/testMap7Large.svg", "test2");
            MySvgReadAndParser.ReadAndAddFile("D:/micha/Documents/MonoGameProjekte/TowerDefenceTEST/Game1/Content/LogicMap/testMap2Large.svg", "test3");
            MySvgReadAndParser.ReadAndAddFile("D:/micha/Documents/MonoGameProjekte/TowerDefenceTEST/Game1/Content/LogicMap/testMap5Large.svg", "test4");
            MySvgReadAndParser.ReadAndAddFile("D:/micha/Documents/MonoGameProjekte/TowerDefenceTEST/Game1/Content/LogicMap/testMap3Large.svg", "test5");
            MySvgReadAndParser.ReadAndAddFile("D:/micha/Documents/MonoGameProjekte/TowerDefenceTEST/Game1/Content/LogicMap/testMap4Large.svg", "test6");
            MySvgReadAndParser.ReadAndAddFile("D:/micha/Documents/MonoGameProjekte/TowerDefenceTEST/Game1/Content/LogicMap/testMap6Large.svg", "test7");

            AddLevel(new TestLevel(1, 1, 1, 8, FontSpriteManager.largeBackground));
            AddLevel(new TestLevel(2, 2, 2, 8, FontSpriteManager.largeBackground7));
            AddLevel(new TestLevel(3, 3, 3, 5, FontSpriteManager.largeBackground2));
            AddLevel(new TestLevel(4, 4, 4, 8, FontSpriteManager.largeBackground5));
            AddLevel(new TestLevel(5, 5, 5, 6, FontSpriteManager.largeBackground3));
            AddLevel(new TestLevel(6, 6, 6, 8, FontSpriteManager.largeBackground4));
            AddLevel(new TestLevel(7, 7, 7, 7, FontSpriteManager.largeBackground6));
        }

        private static void AddLevel(TestLevel newLevel)
        {
            int count = _levels.Count + 1;

            _levels.Add(count, newLevel);
        }


        private static void GetInput()
        {

            if (InputManager.GetSingleKeypress(Keys.N) || HudManager.GetButtonPress("change"))
            {
                DoChangeLevel();
            }
            else if ((InputManager.GetSingleKeypress(Keys.P) || HudManager.GetButtonPress("pause")) && _runGame == true)
            {
                DoPauseLevel();
            }
            else if (InputManager.GetSingleKeypress(Keys.F9) || HudManager.GetButtonPress("restart"))
            {
                DoResetLevel();
            }
            else if ((InputManager.GetSingleKeypress(Keys.F5) || HudManager.GetButtonPress("wave")))
            {
                DoSpawnWave();
            }
            else if ((InputManager.GetSingleKeypress(Keys.M) || HudManager.GetButtonPress("mainmenu")))
            {
                GotoMainMenu();
            }
            else if (InputManager.GetSingleKeypress(Keys.Escape) || HudManager.GetButtonPress("exit"))
            {
                ExitGame = true;
            }
        }

        private static void GotoMainMenu()
        {
            if (_gameBrain.getCurrentState() != null)
            {
                _gameBrain.popState();
            }
            UserTower = new PlayerTower(new Vector2(50, 50), FontSpriteManager.userShip, "userTower", (int)act_Type.TOWER, _currentLevel);
            _lvlsPlayedInRow = -1;
            _gameBrain.pushState("MainMenu");
        }

        private static void DoStartLevel()
        {
            _beginLvlTimer.Start();
            _gameBrain.pushState("StartLevel");
        }

        private static void DoResetLevel()
        {
            if (_resetLvlTimer.ElapsedMilliseconds > _resetLvlCountdown)
            {
                DebugHelper.AddLog(MyMathHelper.GetGuid(), "Try Harder", Color.White);
                _gameBrain.pushState("ResetLevel");
            }
        }

        private static void DoChangeLevel()
        {
            if (_currentLevel.Wave() >= _minWavesToPlay)
            {
                if (_changeLvlTimer.ElapsedMilliseconds > _changeLvlCountdown)
                {
                    _changeLvlTimer.Restart();
                    _runGame = false;
                    _gameBrain.pushState("ChangeLevel");
                }
            }
        }

        private static void DoPauseLevel()
        {
            DebugHelper.AddLog(MyMathHelper.GetGuid(), "---Game Paused---", Color.White);

            _gameBrain.pushState("PauseLevel");
        }

        private static void DoSpawnWave()
        {
            if (_lastState != "PauseLevel")
                if (_currentLevel.EnemyCount() < 1000)
                {
                    _gameBrain.pushState("SpawnWave");
                }
        }


        public static void Update(GameTime gameTime)
        {
            InputManager.Update();
            HudManager.UpdateHud();
            DebugHelper.Update();

            //_testBolt = new LightningBolt(new Vector2(50), new Vector2(1000, 800));

            //if (_testBolt != null)
            //    _testBolt.Update();
            if (_lastState == "PlayLevel" && _currentLevel.EnemyCount() <= 0)
            {
                DoSpawnWave();
            }

            GetInput();
            _lastState = _currentState;
            _currentState = _gameBrain.getCurrentState();

            ExecuteState(_gameBrain.update(), gameTime);

            PlayerCoins = _currentLevel.PlayerCoins;

            _levels[_currentLvlIndex] = _currentLevel;

            AddHistory(_currentState);
        }


        public static void ExecuteState(string state, GameTime gameTime)
        {
            switch (state)
            {
                case "StartLevel":
                    State_StartLevel();
                    break;

                case "PlayLevel":
                    State_PlayLevel(gameTime);
                    break;

                case "ResetLevel":
                    State_ResetLevel();
                    break;

                case "ChangeLevel":
                    State_ChangeLevel();
                    break;

                case "PauseLevel":
                    State_PauseLevel();
                    break;

                case "MainMenu":
                    State_MainMenu();
                    break;

                case "SpawnWave":
                    State_SpawnWave();
                    break;
            }
        }


        public static void State_MainMenu()
        {

            _mainBtnOffset = new Vector2(-1);

            if (HudManager.GetMouseOver("1"))
            {
                _mainBtnOffset = new Vector2(0);

            }
            if (HudManager.GetMouseOver("2"))
            {
                _mainBtnOffset = new Vector2(520, 0);

            }
            if (HudManager.GetMouseOver("3"))
            {
                _mainBtnOffset = new Vector2(1040, 0);

            }
            if (HudManager.GetMouseOver("4"))
            {
                _mainBtnOffset = new Vector2(0, 290);

            }
            if (HudManager.GetMouseOver("5"))
            {
                _mainBtnOffset = new Vector2(520, 290);

            }
            if (HudManager.GetMouseOver("6"))
            {
                _mainBtnOffset = new Vector2(1040, 290);

            }
            if (HudManager.GetMouseOver("7"))
            {
                _mainBtnOffset = new Vector2(0, 580);
            }
            HudManager.UpdateMainMenu();
            if (_lastState != "MainMenu")
            {
                DebugHelper.AddLog(MyMathHelper.GetGuid(), " Choose Level Wise...", Color.Red);
                DebugHelper.AddLog(MyMathHelper.GetGuid(), "", Color.White);
            }
            if ((InputManager.GetSingleKeypress(Keys.D1) || HudManager.GetButtonPress("1")))
            {
                _currentLvlIndex = 0;
                _gameBrain.popState();
                _gameBrain.pushState("ChangeLevel");
                _runGame = true;
                return;
            }
            if ((InputManager.GetSingleKeypress(Keys.D2) || HudManager.GetButtonPress("2")))
            {
                _currentLvlIndex = 1;
                _gameBrain.popState();
                _gameBrain.pushState("ChangeLevel");
                _runGame = true;
                return;
            }
            if ((InputManager.GetSingleKeypress(Keys.D3) || HudManager.GetButtonPress("3")))
            {
                _currentLvlIndex = 2;
                _gameBrain.popState();
                _gameBrain.pushState("ChangeLevel");
                _runGame = true;
                return;
            }
            if ((InputManager.GetSingleKeypress(Keys.D4) || HudManager.GetButtonPress("4")))
            {
                _currentLvlIndex = 3;
                _gameBrain.popState();
                _gameBrain.pushState("ChangeLevel");
                _runGame = true;
                return;
            }
            if ((InputManager.GetSingleKeypress(Keys.D5) || HudManager.GetButtonPress("5")))
            {
                _currentLvlIndex = 4;
                _gameBrain.popState();
                _gameBrain.pushState("ChangeLevel");
                _runGame = true;
                return;
            }
            if ((InputManager.GetSingleKeypress(Keys.D6) || HudManager.GetButtonPress("6")))
            {
                _currentLvlIndex = 5;
                _gameBrain.popState();
                _gameBrain.pushState("ChangeLevel");
                _runGame = true;
                return;
            }
            if ((InputManager.GetSingleKeypress(Keys.D7) || HudManager.GetButtonPress("7")))
            {
                _currentLvlIndex = 6;
                _gameBrain.popState();
                _gameBrain.pushState("ChangeLevel");
                _runGame = true;
                return;
            }

            _lightEmitter.Update(new Vector2(50, 100) + _mainBtnOffset, DrawHelper.GetRandomColor());
            _lightEmitter.AddTarget(new Vector2(550, 100) + _mainBtnOffset);
            _lightEmitter.AddTarget(new Vector2(50, 370) + _mainBtnOffset);
            _lightEmitter.Update(new Vector2(550, 370) + _mainBtnOffset, DrawHelper.GetRandomColor());
            _lightEmitter.AddTarget(new Vector2(550, 100) + _mainBtnOffset);
            _lightEmitter.AddTarget(new Vector2(50, 370) + _mainBtnOffset);


            _lightEmitter.Update(new Vector2(50, 100) + _mainBtnOffset, DrawHelper.GetRandomColor());
            _lightEmitter.AddTarget(new Vector2(550, 100) + _mainBtnOffset);
            _lightEmitter.AddTarget(new Vector2(50, 370) + _mainBtnOffset);
            _lightEmitter.Update(new Vector2(550, 370) + _mainBtnOffset, DrawHelper.GetRandomColor());
            _lightEmitter.AddTarget(new Vector2(550, 100) + _mainBtnOffset);
            _lightEmitter.AddTarget(new Vector2(50, 370) + _mainBtnOffset);
        }

        private static void State_StartLevel()
        {
            if (_beginLvlTimer.ElapsedMilliseconds <= 0)
            {
                DoStartLevel();
            }
            if (_beginLvlTimer.ElapsedMilliseconds >= _startCountdown)
            {
                _currentLevel.Initialize(_currentLevel.Id);
                _lvlsPlayedInRow++;
                _currentLevel.PlayerCoins += _lvlsPlayedInRow * 20;
                _beginLvlTimer.Reset();
                _gameBrain.popState();
                _gameBrain.pushState("PlayLevel");
            }
        }

        private static void State_PlayLevel(GameTime gameTime)
        {

            _currentLevel.Update(gameTime);

        }

        private static void State_PauseLevel()
        {
            if ((InputManager.GetSingleKeypress(Keys.P) || HudManager.GetButtonPress("pause")))
            {
                if (_runGame == false)
                {
                    _gameBrain.popState();
                    _runGame = true;
                    return;
                }
            }
            _runGame = false;
        }

        private static void State_SpawnWave()
        {
            if (_currentState != _lastState)
            {
                _currentLevel.SpawnWave();
            }
            _gameBrain.popState();
        }

        private static void State_ResetLevel()
        {
            HudManager.ResetHud();
            UserTower.setPosition(new Vector2(-50, -50));
            _lvlsPlayedInRow--;

            CreateUiButtons();
            _currentLevel.Reset();
            _runGame = true;
            _beginLvlTimer.Reset();
            _resetLvlTimer.Restart();

            PlayerCoins = _coinsLastLvl;
            _gameBrain.pushState("StartLevel");
        }

        private static void State_ChangeLevel()
        {
            if (_currentLvlIndex < _levels.Count)
            {
                _currentLvlIndex++;
            }
            else
            {
                _currentLvlIndex = 1;
            }

            HudManager.ResetHud();
            UserTower.setPosition(new Vector2(-50, -50));
            CreateUiButtons();
            _coinsLastLvl = _currentLevel.PlayerCoins;
            PlayerCoins = _coinsLastLvl;
            _currentLevel = _levels[_currentLvlIndex];
            _currentLevel.Reset();
            _runGame = true;
            _beginLvlTimer.Reset();
            _changeLvlTimer.Restart();
            _gameBrain.pushState("StartLevel");
        }


        public static void Draw(SpriteBatch spriteBatch)
        {
            switch (_gameBrain.getCurrentState())
            {
                case "StartLevel":
                    DrawStart(spriteBatch);
                    break;

                case "ResetLevel":
                    DrawLevel(spriteBatch);
                    break;

                case "PlayLevel":
                    DrawLevel(spriteBatch);
                    break;

                case "PauseLevel":
                    DrawPause(spriteBatch);
                    break;

                case "MainMenu":
                    DrawMainMenu(spriteBatch);
                    break;
            }
            DebugHelper.DrawLogWithColor(spriteBatch);
            DrawMouse(spriteBatch);
        }

        private static void DrawMainMenu(SpriteBatch spriteBatch)
        {
            if (_mainBtnOffset != new Vector2(-1))
            {
                _lightEmitter.Draw(spriteBatch);

            }
            HudManager.DrawMainMenu(spriteBatch);
        }

        private static void DrawLevel(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_currentLevel._background, new Vector2(0, 0), Color.White);
            DrawHelper.DrawFilledRectangle(spriteBatch, new Vector2(1600, 0), new Vector2(200, 1000), 0, Color.Black);
            _currentLevel.Draw(spriteBatch);
            DrawHudButtons(spriteBatch);
            DrawGameInfo(spriteBatch);
        }

        private static void DrawStart(SpriteBatch spriteBatch)
        {
            DrawLevel(spriteBatch);
            DrawHudButtons(spriteBatch);

            Vector2 size = new Vector2(460, 60);
            Vector2 location = new Vector2(520, 395);
            int timeInSec = (int)(_startCountdown - _beginLvlTimer.ElapsedMilliseconds) / 1000;

            DrawHelper.DrawTextWithLabel(spriteBatch, location, size, "Prepare for Invasion " + timeInSec, 1, Color.CadetBlue);
        }

        private static void DrawPause(SpriteBatch spriteBatch)
        {
            DrawLevel(spriteBatch);
            DrawHudButtons(spriteBatch);
            //DrawGameInfo(spriteBatch);
            DrawHelper.DrawFilledRectangle(spriteBatch, new Vector2(685, 395), new Vector2(140, 50), 0, Color.Blue);
            DrawHelper.DrawEmptyRectangle(spriteBatch, new Vector2(685, 395), new Vector2(140, 50), 3, Color.Black);
            DrawHelper.DrawText(spriteBatch, "Pause", new Vector2(700, 400), 1, 0, Color.WhiteSmoke);

            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(700, 450), new Vector2(110, 30), "Level: " + _currentLvlIndex.ToString(), 1f, Color.CadetBlue);
            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(700, 480), new Vector2(110, 30), "Creeps: " + EnemyCount().ToString(), 1f, Color.CadetBlue);
            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(700, 510), new Vector2(110, 30), "Coins: " + PlayerCoins.ToString(), 1f, Color.CadetBlue);
        }


        private static void DrawHudButtons(SpriteBatch spriteBatch)
        {
            HudManager.Draw(spriteBatch);
        }

        private static void DrawGameInfo(SpriteBatch spriteBatch)
        {
            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(1610, 75), new Vector2(140, 30), "Level: " + _currentLvlIndex.ToString(), 1f, Color.AntiqueWhite);
            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(1610, 100), new Vector2(140, 30), "Done: " + _lvlsPlayedInRow, 1f, Color.AntiqueWhite);

            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(1610, 125), new Vector2(140, 30), "Creeps: " + EnemyCount().ToString(), 1f, Color.AntiqueWhite);
            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(1610, 150), new Vector2(140, 30), "Coins: " + PlayerCoins.ToString(), 1f, Color.AntiqueWhite);
            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(1610, 175), new Vector2(140, 30), "Wave: " + (_currentLevel.Wave() - 1), 1f, Color.AntiqueWhite);
            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(1610, 200), new Vector2(140, 30), "Fails: " + _currentLevel.Failures, 1f, Color.AntiqueWhite);
            DrawHelper.DrawSmallTextWithLabel(spriteBatch, new Vector2(1610, 225), new Vector2(140, 30), InputManager.GetScrollValue().ToString(), 1f, Color.AntiqueWhite);

        }

        private static void DrawMouse(SpriteBatch spriteBatch)
        {
            DrawHelper.DrawSmallText(spriteBatch, "[   +   ]", (InputManager.GetMouse().Position - new Point(32, 10)).ToVector2(), 1, 0, Color.White);
        }


        private static void AddHistory(string state)
        {
            if (_stateHistory.Count > 30)
                _stateHistory.RemoveAt(0);
            if (_lastState != state)
                _stateHistory.Add(state);
            else if (_stateHistory.Count == 0)
                _stateHistory.Add(state);

            _lastState = state;
        }

        public static int EnemyCount()
        {
            return _currentLevel.EnemyCount();
        }

        public static int ProjectileCount()
        {
            return _currentLevel.ProjectileCount();
        }

    }
}