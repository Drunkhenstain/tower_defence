﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;


namespace Game1
{
    public static class HudManager
    {
        private static Dictionary<string, Button> _mainMenuButtons = new Dictionary<string, Button>();
        private static Dictionary<string, Button> _hudButtons = new Dictionary<string, Button>();
        private static Dictionary<string, Button> _backgroundButtons = new Dictionary<string, Button>();
        private static Dictionary<string, Button> _playerButtons = new Dictionary<string, Button>();


        public static bool HudButtonPressed;
        private static int _frameCountBtnPressed;

        public static void UpdateHud()
        {
            if (_frameCountBtnPressed > 10)
            {
                _frameCountBtnPressed = 0;
                HudButtonPressed = false;
            }
            if (HudButtonPressed == true)
            {
                _frameCountBtnPressed++;
            }
            foreach (Button btn in _backgroundButtons.Values)
            {
                btn.Update();
                if (btn.IsClicked() == true || btn.GetsInput() == true)
                {
                    _frameCountBtnPressed = 0;
                    HudButtonPressed = true;
                }
            }

            foreach (Button btn in _hudButtons.Values)
            {
                btn.Update();
                if (btn.IsClicked() == true || btn.GetsInput() == true)
                {
                    _frameCountBtnPressed = 0;
                    HudButtonPressed = true;
                }
            }
        }

        public static void UpdateMainMenu()
        {
            foreach (Button btn in _mainMenuButtons.Values)
            {
                btn.Update();
                if (btn.IsClicked() == true)
                {
                    HudButtonPressed = true;
                }
            }
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            foreach (Button btn in _hudButtons.Values)
            {
                btn.Draw(spriteBatch);
            }
        }

        public static void DrawBackgrounds(SpriteBatch spriteBatch)
        {
            foreach (Button btn in _backgroundButtons.Values)
            {
                btn.Draw(spriteBatch);
            }
        }

        public static void DrawMainMenu(SpriteBatch spriteBatch)
        {
            foreach (Button btn in _mainMenuButtons.Values)
            {
                btn.Draw(spriteBatch);
            }
        }

        public static bool GetButtonPress(string Key)
        {
            if (_hudButtons.ContainsKey(Key))
                return _hudButtons[Key].IsClicked();
            else if (_mainMenuButtons.ContainsKey(Key))
                return _mainMenuButtons[Key].IsClicked();
            else if (_backgroundButtons.ContainsKey(Key))
                return _backgroundButtons[Key].IsClicked();
            else
                return false;
        }


        public static bool GetMouseOver(string Key)
        {
            if (_hudButtons.ContainsKey(Key))
                return _hudButtons[Key].MouseOver();
            else if (_mainMenuButtons.ContainsKey(Key))
                return _mainMenuButtons[Key].MouseOver();
            else if (_backgroundButtons.ContainsKey(Key))
                return _backgroundButtons[Key].MouseOver();
            else
                return false;
        }

        public static bool ContainsButton(string Key)
        {
            bool contains = _hudButtons.ContainsKey(Key);

            if (contains == false)
            {
                contains = _mainMenuButtons.ContainsKey(Key);
            }
            if (contains == false)
            {
                contains = _backgroundButtons.ContainsKey(Key);
            }

            return contains;
        }

        public static void AddButton(string Key, Button Button)
        {
            _hudButtons.Add(Key, Button);
        }
        public static void RemoveButton(string Key)
        {
            _hudButtons.Remove(Key);
        }

        public static void AddMainMenuButton(string Key, Button Button)
        {
            _mainMenuButtons.Add(Key, Button);
        }

        public static void RemoveMainMenuButton(string Key)
        {
            _mainMenuButtons.Remove(Key);
        }

        public static void AddBackgroundButton(string Key, Button Button)
        {
            _backgroundButtons.Add(Key, Button);
        }

        public static void RemoveBackgroundButton(string Key)
        {
            _backgroundButtons.Remove(Key);
        }

        public static void ResetHud()
        {
            _hudButtons.Clear();
            _backgroundButtons.Clear();
        }
    }
}
