﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game1
{
    public static class InputManager
    {
        static MouseState _mouseState = new MouseState();
        static KeyboardState _keyboardState = new KeyboardState();

        static MouseState _lastMState = new MouseState();
        static KeyboardState _lastKState = new KeyboardState();

        static int _inputCount;
        static int _lastScrollValue = 0;

        public static void Update()
        {
            _inputCount = 0;
            _lastMState = _mouseState;
            _lastKState = _keyboardState;

            _mouseState = Mouse.GetState();
            _keyboardState = Keyboard.GetState();
        }

        public static MouseState GetMouse()
        {
            return _mouseState;
        }

        public static KeyboardState GetKeys()
        {
            return _keyboardState;
        }

        public static int GetScrollValue()
        {
            int value = 0;

            if (_lastScrollValue < GetMouse().ScrollWheelValue)
            {
                value = 1;
            }
            else if (_lastScrollValue > GetMouse().ScrollWheelValue)
            {
                value = -1;
            }
            else
            {
                value = 0;
            }
            _lastScrollValue = GetMouse().ScrollWheelValue;
            return value;

        }

        public static bool GetSingleKeypress(Keys key)
        {
            if (_keyboardState.IsKeyDown(key) & !_lastKState.IsKeyDown(key))
            {
                return true;
            }
            else
                return false;
        }

        public static bool IsLeftMousePressed()
        {
            if (_mouseState.LeftButton == ButtonState.Pressed)
            {
                _inputCount++;
                return true;
            }
            else
                return false;
        }

        public static bool IsRightMousePressed()
        {
            if (_mouseState.RightButton == ButtonState.Pressed)
            {
                _inputCount++;
                return true;
            }
            else
                return false;
        }

        public static int InputCount()
        {
            return _inputCount;
        }
    }
}
