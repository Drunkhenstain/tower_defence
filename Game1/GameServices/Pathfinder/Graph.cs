﻿using System;
using System.Collections.Generic;

namespace Game1
{
	public class Graph
	{
		List<Vertex> vertexes;
		List<Edge> edges;

		public Graph(List<Vertex>vertexes,List<Edge>edges)
		{
			this.vertexes = vertexes;
			this.edges = edges;
		}

		public List<Vertex> getVertexes()
		{
			return vertexes;
		}

		public List<Edge> getEdges()
		{
			return edges;
		}
	}
}
