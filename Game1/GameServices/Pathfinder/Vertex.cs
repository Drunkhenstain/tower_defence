﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
	public class Vertex:IComparable
	{
		String id;
		int value;

		int posX;
		int posY;

		public int getX()
		{
			return posX;
		}

		public int getY()
		{
			return posY;
		}

		public int getValue()
		{
			return value;
		}

		public string getID()
		{
			return id;
		}

        public Point toPoint()
        {
            return new Point(posX, posY);
        }

        public Vector2 toVector2()
        {
            return new Vector2(posX, posY);
        }

		public int CompareTo(object obj)
		{
			throw new NotImplementedException();
		}

		public Vertex(int posX,int posY,int value)
		{
			id += posX;
            id += ".";
			id += posY;
			this.posX = posX;
			this.posY = posY;
			this.value = value;
		}
	}
}
