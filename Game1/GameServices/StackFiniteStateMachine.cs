﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    class StackFiniteStateMachine
    {
        private Stack<string> stateStack;

        public StackFiniteStateMachine()
        {
            stateStack = new Stack<string>();
        }

        public string update()
        {
            string currentState = getCurrentState();
            if (currentState != null)
                return currentState;
            else
                return "";
        }

        public string popState()
        {
            return stateStack.Pop();
        }

        public void pushState(string state)
        {
            if (stateStack.Count == 0)
                stateStack.Push(state);
            else if (state != stateStack.Peek())
            {
                stateStack.Push(state);
            }
        }

        public string getCurrentState()
        {
            if (stateStack.Count > 0)
                return stateStack.Peek();
            else
                return null;
        }
    }
}
