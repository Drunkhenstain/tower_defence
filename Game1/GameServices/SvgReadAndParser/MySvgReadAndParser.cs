﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Game1
{
    public static class MySvgReadAndParser
    {
        private static Dictionary<string, SvgDataContainer> levelData = new Dictionary<string, SvgDataContainer>();
        private static MySvgParser svgParser;
        private static RawPathToPolylineParser plParser = new RawPathToPolylineParser();

        public static void ReadAndAddFile(string filePath, string fileDescription)
        {
            List<Polyline> finalPaths = new List<Polyline>();
            List<Vector2> finalCircles = new List<Vector2>();
            List<Vector2> finalRectangles = new List<Vector2>();
            SvgDataContainer finalData;

            svgParser = null;
            svgParser = new MySvgParser(filePath);

            finalPaths = plParser.getConvertedPaths(svgParser.rawPaths);
            finalCircles = plParser.getConvertedCircles(svgParser.rawCircles);
            finalRectangles = plParser.getConvertedRectangles(svgParser.rawRectangles);

            finalData = new SvgDataContainer(fileDescription, finalPaths, finalCircles,finalRectangles);

            levelData.Add(fileDescription, finalData);
        }

        public static SvgDataContainer GetLatestData()
        {
            if (levelData.Count > 0)
                return levelData.ElementAt(levelData.Count - 1).Value;
            else
                return null;
        }

        public static SvgDataContainer GetDataWithDescription(string fileDescription)
        {
            if (levelData.Count > 0)
                return levelData[fileDescription];
            else
                return null;
        }
    }
}
