﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game1
{
    using System;
    using System.Xml;
    class MySvgParser
    {

        public string resolution = "";
        public List<string> rawPolylines = new List<string>();
        public List<string> rawPaths = new List<string>();
        public List<string> rawCircles = new List<string>();
        public List<string> rawRectangles = new List<string>();
        public MySvgParser(string xmlPath)
        {
            /// Prepare Specific Settings for SVGs
            /// To Avoid GodDamn Exeptions
            /// 
            XmlReaderSettings settings = new XmlReaderSettings();
            /// IGNORE--> settings.ConformanceLevel = ConformanceLevel.Fragment;
            /// 
            settings.IgnoreComments = true;
            /// Create a new NameTable
            /// 
            NameTable nt = new NameTable();
            /// Create a new NamespaceManager
            /// 
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(nt);
            /// Add your namespaces used in the XML
            /// 
            nsmgr.AddNamespace("xlink", "urn:http://www.w3.org/2000/svg");
            settings.DtdProcessing = DtdProcessing.Ignore;
            /// Create the XmlParserContext using the previous declared XmlNamespaceManager
            /// 
            XmlParserContext ctx = new XmlParserContext(null, nsmgr, null, XmlSpace.None);
            using (XmlReader reader = XmlReader.Create(xmlPath, settings, ctx))
            {
                while (reader.Read())
                {
                    /// Only detect start elements.
                    /// 
                    if (reader.IsStartElement())
                    {
                        /// Get element name and switch on it.
                        /// 
                        switch (reader.Name)
                        {
                            case "svg":
                                resolution += "width: ";
                                resolution += reader.GetAttribute("width");
                                resolution += " heigth: ";
                                resolution += reader.GetAttribute("height");
                                break;

                            case "polyline":
                                rawPolylines.Add(reader.GetAttribute("points"));
                                break;

                            case "path":
                                rawPaths.Add(reader.GetAttribute("d"));
                                break;

                            case "ellipse":
                                rawCircles.Add(string.Concat(reader.GetAttribute("cx"),",", reader.GetAttribute("cy")));
                                break;

                            case "circle":
                                rawCircles.Add(string.Concat(reader.GetAttribute("cx"), ",", reader.GetAttribute("cy")));
                                break;

                            case "rect":
                                rawRectangles.Add(string.Concat(reader.GetAttribute("x"), ",", reader.GetAttribute("y")));
                                break;
                        }
                    }
                }
            }
        }
    }
}
