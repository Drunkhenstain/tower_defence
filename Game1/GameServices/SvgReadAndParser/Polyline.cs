﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public class Polyline
    {
        public List<Vector2> polylines = new List<Vector2>();

        public void addPoint(Vector2 point)
        {
            polylines.Add(point);
        }
        public bool isEmpty()
        {
            if(polylines.Count>0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public int lastIndex()
        {
            return polylines.Count - 1;
        }
    }
}
