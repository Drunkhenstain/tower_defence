﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    public class RawPathToPolylineParser
    {
        public List<Vector2> getConvertedRectangles(List<string> rawRectangles)
        {
            List<Vector2> finalRectangles = new List<Vector2>();
            foreach (string rectangle in rawRectangles)
            {
                Vector2 tempRect;

                float x = int.MinValue;
                float y = int.MinValue;

                foreach (string coordinate in rectangle.Split(','))
                {
                    if (x == int.MinValue)
                    {
                        x = Convert.ToSingle(coordinate, System.Globalization.NumberFormatInfo.InvariantInfo);
                    }
                    else if (y == int.MinValue)
                    {
                        y = Convert.ToSingle(coordinate, System.Globalization.NumberFormatInfo.InvariantInfo);
                    }
                }
                tempRect= new Vector2(x, y);
                finalRectangles.Add(tempRect);
            }
            return finalRectangles;
        }

        public List<Vector2> getConvertedCircles(List<string> rawCircles)
        {
            List<Vector2> finalCircles = new List<Vector2>();
            foreach (string circle in rawCircles)
            {
                Vector2 tempCircle;
                float cx = int.MinValue;
                float cy = int.MinValue;               

                foreach (string coordinate in circle.Split(','))
                {
                    if (cx == int.MinValue)
                    {
                        cx = Convert.ToSingle(coordinate, System.Globalization.NumberFormatInfo.InvariantInfo);
                    }
                    else if (cy == int.MinValue )
                    {
                        cy = Convert.ToSingle(coordinate, System.Globalization.NumberFormatInfo.InvariantInfo);
                    }
                }
                tempCircle = new Vector2(cx, cy);
                finalCircles.Add(tempCircle);
            }
            return finalCircles;
        }

        public List<Polyline> getConvertedPaths(List<string> rawData)
        {
            List<Polyline> finalLines = new List<Polyline>();
            foreach (string polyline in rawData)
            {
                Polyline tempLine = new Polyline();
                string rawXSource = "";
                string rawYSource = "";
                bool isAbsolute = true;
                bool isXValue = true;
                string mode = "";
                foreach (string coordinates in polyline.Split(' '))
                {
                    // only works with the vsg-m-z-h-v-l modifier
                    
                    if (coordinates == "z")
                    {
                        mode = "m";
                        tempLine.addPoint(tempLine.polylines[0]);
                    }
                    else if (coordinates == "Z")
                    {
                        mode = "m";
                        tempLine.addPoint(tempLine.polylines[0]);
                    }
                    else if (coordinates == "M")
                    {
                        mode = "m";
                        isAbsolute = true;
                    }
                    else if (coordinates == "m")
                    {
                        mode = "m";
                        isAbsolute = false;
                    }
                    else if (coordinates == "L")
                    {
                        mode = "m";
                        isAbsolute = true;
                    }
                    else if (coordinates == "l")
                    {
                        mode = "m";
                        isAbsolute = false;
                    }
                    else if (coordinates == "h")
                    {
                        mode = "h";
                        isAbsolute = false;
                    }
                    else if (coordinates == "H")
                    {
                        mode = "h";
                        isAbsolute = true;
                    }
                    else if (coordinates == "v")
                    {
                        mode = "v";
                        isAbsolute = false;
                    }
                    else if (coordinates == "V")
                    {
                        mode = "v";
                        isAbsolute = true;
                    }
                    else
                    {
                        Vector2 finalPoint;
                        if (mode == "m")
                        {
                            foreach (string xyValue in coordinates.Split(','))
                            {

                                if (rawXSource == "" && isXValue == true)
                                {
                                    rawXSource = xyValue;
                                    isXValue = false;
                                }
                                else if (rawYSource == "" && isXValue == false)
                                {
                                    rawYSource = xyValue;
                                    isXValue = true;
                                }
                            }
                        }
                        else if (mode == "h")
                        {
                            rawXSource = coordinates;
                            rawYSource = tempLine.polylines[tempLine.lastIndex()].Y.ToString();
                        }
                        else if (mode == "v")
                        {
                            rawXSource = tempLine.polylines[tempLine.lastIndex()].X.ToString();
                            rawYSource = coordinates;
                        }
                        float finalX = Convert.ToSingle(rawXSource, System.Globalization.NumberFormatInfo.InvariantInfo);
                        float finalY = Convert.ToSingle(rawYSource, System.Globalization.NumberFormatInfo.InvariantInfo);

                        if (isAbsolute == true)
                        {
                            finalPoint = new Vector2(finalX, finalY);
                        }
                        else
                        {
                            if (tempLine.isEmpty() == true)
                            {
                                finalPoint = new Vector2(finalX, finalY);
                            }
                            else
                            {
                                Vector2 relPoint = new Vector2(finalX, finalY);
                                Vector2 lastPoint = tempLine.polylines[tempLine.lastIndex()];
                                float relX = lastPoint.X + relPoint.X;
                                float relY = lastPoint.Y + relPoint.Y;

                                if(mode=="v")
                                {
                                    relX = lastPoint.X;
                                    relY = lastPoint.Y + relPoint.Y;
                                }
                                else if(mode=="h")
                                {
                                    relX = lastPoint.X + relPoint.X;
                                    relY = lastPoint.Y;
                                }
                                finalPoint = new Vector2(relX, relY);
                            }
                        }
                        tempLine.addPoint(finalPoint);
                        rawXSource = "";
                        rawYSource = "";
                    }
                }
                finalLines.Add(tempLine);
            }
            return finalLines;
        }
    }
}
