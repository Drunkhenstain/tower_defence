﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace Game1
{

    public class SvgDataContainer
    {
        public string Description { get; private set; }
        public List<Polyline> Paths { get; private set; }
        public List<Vector2> Circles { get; private set; }
        public List<Vector2> Rectangles { get; private set; }

        public SvgDataContainer(string description, List<Polyline> paths, List<Vector2> circles, List<Vector2> rectangles)
        {
            Description = description;
            Paths = paths;
            Circles = circles;
            Rectangles = rectangles;
        }
    }
}
